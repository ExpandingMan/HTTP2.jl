using HTTP2
using Documenter

DocMeta.setdocmeta!(HTTP2, :DocTestSetup, :(using HTTP2); recursive=true)

makedocs(;
    modules=[HTTP2],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/HTTP2.jl/blob/{commit}{path}#{line}",
    sitename="HTTP2.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/HTTP2.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "Internals" => ["internals/hpack.md"],
    ],
)
