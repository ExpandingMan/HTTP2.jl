```@meta
CurrentModule = HTTP2
```

# HPACK
[HPACK](https://httpwg.org/specs/rfc7541.html) is the HTTP 2 header compression scheme.
This is a specialized algorithm and does not allow for arbitrary compression schemes.

HPACK is implemented in the module `HTTP2.HPack`.

Header compression and decompression is implemented by compressing or decompressing data
as it is first inserted to or read from an IO stream.  As the specification is rigid and
does not allow for any sort of custom data or metadata, the implementation is not
particularly flexible.

## API
```@autodocs
Modules = [HTTP2.HPack]
Filter = t -> !startswith(string(t), "_")
Order = [:module, :type, :constant, :function, :macro]
```
