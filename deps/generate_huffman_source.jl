#========================================================================================
This script is used to generate Julia source code from the Huffman code table found in
the HTTP 2 specification.

Rather unbelievably, I could not find a more easily parseable format of this table
anywhere online
========================================================================================#

readhuffman(fname::AbstractString="hpack_huffman.txt") = open(readhuffman, fname)
function readhuffman(io::IO)
    tbl = (sym=UInt16[], code=UInt32[], length=Int[])
    for l ∈ eachline(io)
        huffmanline!(tbl, l)
    end
    tbl
end

function huffmanline!(tbl, l::AbstractString)
    # table as provided is justified thusly
    push!(tbl.sym, parse(UInt16, l[6:8]))
    push!(tbl.code, parse(UInt32, "0x"*strip(l[50:59])))
    push!(tbl.length, parse(Int, l[63:64]))
end
