#============================================================================================================
This script is used to generate Julia source code from the static table found in the HTTP 2 specification.

Fortunately the HTML table is formatted, so thetext file is obtained simply by copying and pasting.
============================================================================================================#
using DelimitedFiles

function readtable(fname::AbstractString="hpack_statictable.txt")
    tbl = readdlm(fname)
    tbl = (index=Int.(tbl[:, 1]),
           header_name=string.(tbl[:, 2]),
           value=string.(tbl[:, 3]) .* string.(tbl[:, 4]),
          )
end

function genpairs!(io::IO, tbl)
    for i ∈ 1:length(tbl.index)
        write(io, "$(tbl.index[i]) => (\"$(tbl.header_name[i])\" => \"$(tbl.value[i])\"),\n")
    end
end
genpairs!(fname::AbstractString, tbl=readtable()) = open(io -> genpairs!(io, tbl), fname, write=true)
