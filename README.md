# HTTP2

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/HTTP2.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/HTTP2.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/HTTP2.jl/-/pipelines)

## **THIS PROJECT IS NO LONGER UNDER DEVELOPMENT**
This was a project I started to replace HTTP.jl, though I have ultimately decided not to pursue it
further.  Features distinguishing this implementation are:
- Full implementation of the HTTP 2 protocol in Julia.
- Full implementation of HPACK header serialization protocol, *THIS IS COMPLETE AND TESTED!*
- Parallel reading and writing from a single connection as allowed by the HTTP 2 protocol.
- Backward compatible with HTTP 1.1 by using "virtual frames".  In other words, the implementation
    is like a generalization of HTTP 2 that includes a special frame type for support of HTTP 1.1.

The biggest set of features that is missing from the current repo is connection pools, which have
not been implemented at all.  I *think* the single-connection implementation is more-or-less
in principle but it requires a ton of work to make it reliable in realistic use casees.

While I don't intend to work on this anymore myself, it is my hope that others will find it helpful
and that one day we will have a working Julia implementation of HTTP 2.


## Information
The complete HTTP 2 specification can be found
[here](https://httpwg.org/specs/rfc7540.html).  A useful summary article can be found
[here](https://hpbn.co/http2/).

## Testing
Some useful sites for testing:
- [http2.pro](https://http2.pro/)
- [httpbin](https://github.com/postmanlabs/httpbin), instances: https://httpbin.org
