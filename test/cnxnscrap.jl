using HTTP, Sockets, Debugger, MbedTLS, URIs
using Base.Threads: @spawn
#HTTP.DEBUG_LEVEL[] = 1000  # this rarely prints any useful information

# testing on connection to local httpbin instance
#s = connect(Sockets.localhost, 80)

str = "GET / HTTP/1.1\r\nHost: localhost\r\nAccept: */*\r\nUser-Agent: HTTP.jl/1.6.0-rc1\r\nContent-Length: 0\r\n\r\n"

# this should be the minimal HTTP stack that works
function stack()
    MessageLayer{ConnectionPoolLayer{StreamLayer{Union{}}}}
end

# a request with the minimal stack
function request(method="GET", url="http://localhost:80")
    HTTP.request(stack(), method, HTTP.request_uri(url, nothing), HTTP.mkheaders(HTTP.Header[]), UInt8[],
                 verbose=2)
end

function httptestserver(port=8081)
    HTTP.serve(Sockets.localhost, port) do r
        @show r
        HTTP.Response(404)
    end
end

