"""
    CLCurl

A module wrapping some simple commands of command line curl.

This particular module is used to provide some easy external tests for HTTP2.jl and is
particularly focused on HTTP v2 related functionality.
"""
module CLCurl

function requestcmd(method::AbstractString, url::AbstractString, headers=[], body=""; http_version="http2")
    c = ["curl", "-i", "--silent", "--$http_version", "-X", method]
    for h ∈ headers
        append!(c, ["-H", "$(first(h)): $(last(h))"])
    end
    isempty(body) || append!(c, ["--data-raw", body])
    push!(c, url)
    cmd = Cmd(c)
    @debug("prepared curl command:", cmd)
    cmd
end

"""
    reqest(method, url, headers=[], body=""; wait=false, http_version="http2")

Make an HTTP request to `url` using method `method` with additional headers `headers` (any iterator of pairs of strings) and body
`body`.  `http_version` gives the HTTP protocol version used and should be specified the way it is to `curl` command line arguments
(e.g. `http1.1`, `http2`, `http2-prior-knowledge`).  An `IOBuffer` object to which `curl` output is written will be returned.

If `wait` then the thread will block until the `curl` process exits.  Note that if `wait=false`, no guarantees can be made about what will appear
in the returned buffer.
"""
function request(args...; wait::Bool=true, kwargs...)
    io = IOBuffer()
    cmd = requestcmd(args...; kwargs...)
    run(pipeline(cmd, stdout=io); wait)
    io
end

get(args...; kwargs...) = request("GET", args...; kwargs...)
put(args...; kwargs...) = request("PUT", args...; kwargs...)
post(arsg...; kwargs...) = request("POST", args...; kwargs...)

end
