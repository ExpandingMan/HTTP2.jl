using HTTP2, Sockets

using Base.Threads: @spawn

const rawlisten_channel = Channel{Vector{UInt8}}()


"""
    rawlisten(χ::Channel, port=8000)

Listen for connections on port `port` dumping all data sent to it into the channel `χ`.
"""
function rawlisten(χ::Channel, port::Integer=8000)
    while true
        s = listen(Sockets.localhost, port)
        io = accept(s)
        @info("accepted connection on port $port")
        while isopen(io)
            v = readavailable(io)
            @info("got message on port $port:", v)
            put!(χ, v)
        end
    end
end
rawlisten(port::Integer=8000) = rawlisten(rawlisten_channel, port)

striphttp2preface(v::AbstractVector{UInt8}) = v[25:end]
