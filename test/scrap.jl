includet("CLCurl.jl")
includet("RawIntercept.jl")
using .RawIntercept
using .RawIntercept: serve
using Debugger, BenchmarkTools, HTTP, URIs

using Base.Threads: @spawn

using HTTP2, Sockets

using HTTP2: Frame, FrameHeader, FramePayload, DataPayload, HeadersPayload, SettingsPayload, UnpackedHeadersPayload, ClientSettingsPayload
using HTTP2: Message, FrameIterator, LegacyPayload, IOReadRecorder, IOWriteRecorder, IORecorder
using HTTP2: readiteration, writeiteration, putoutgoing!, request, response

using HTTP2: Connection


ENV["JULIA_DEBUG"] = "HTTP2"


Base.kill(t::Task) = Base.throwto(t, InterruptException())

function maketest(;legacy=false)
    Connection("http://localhost:8000", IOBuffer(); legacy)
end

testmessage() = Message(request, "http://localhost:8000", "GET")

testresponse() = Message("response", [":status"=>"200"])

function make_legacy_frame()
    m = Message("test message", [":method"=>"GET", ":path"=>"/"])
    f = HTTP2.legacyframe(m)
end

function testserver_raw(;run::Bool=true, kw...)
    tcps = listen(Sockets.localhost, 8000)
    io = accept(tcps)
    χ = Connection("http://localhost:8000", io; kw...)
    run && HTTP2.run!(χ)
    χ
end

function testserver(;run::Bool=true, kw...)
    if run
        HTTP2.serve(m -> testresponse(), 8000; kw...)
    else
        HTTP2.serve(8000; kw...)
    end
end

testclient(port=8000; kw...) = Connection("http://localhost:$port"; kw...)

function framesfrommessage(m::Message)
    fi = HTTP2.FrameIterator(m, 1)
    collect(fi)
end

function test1()
    str = """
    HTTP/1.1 101 Message\r
    Test1: abc\r
    Upgrade: h2c\r
    Fuck: you\r
    \r
    Some more text\r
    """
    read(IOBuffer(str), LegacyPayload)
end

#========================================================================================================
    TODO:
    - finish fixing overhauled legacy stuff
    - legacy is over-reading if Content-length is missing
    - make sure connections close more elegantly
========================================================================================================#
