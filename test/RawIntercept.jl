module RawIntercept

using Sockets, URIs
using Base.Threads: @spawn


mutable struct Interceptor
    χ::Channel{Vector{UInt8}}
    t::Union{Nothing,Task}
    port::Int
end

Interceptor(port::Integer) = Interceptor(Channel{Vector{UInt8}}(Inf), nothing, port)

function serve(I::Interceptor)
    s = listen(Sockets.localhost, I.port)
    @info("Interceptor will listen on port $(I.port)")
    I.t = @spawn while true
        io = accept(s)
        @info("accepted connection on port $(I.port)")
        while isopen(io)
            # at some point may want some HTTP-specific detection of whether a message is "done"
            msg = readavailable(io)
            @debug("received message of length $(length(msg)) on port $(I.port)")
            put!(I.χ, msg)
        end
    end
    I
end

Base.fetch(I::Interceptor) = fetch(I.χ)
Base.isready(I::Interceptor) = isready(I.χ)
Base.take!(I::Interceptor) = take!(I.χ)

Base.kill(I::Interceptor) = (Base.throwto(I.t, InterruptException()); I.t = nothing)


export Interceptor

end
