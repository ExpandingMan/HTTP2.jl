using HTTP2, HTTP2.HPack
using Debugger, Test

using HTTP2.HPack: DecodeTable, Decoder, readentry, tablesize, entry!


function spec_request_decode_huffman_1()
    d = Decoder()

    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x82, 0x86, 0x84, 0x41, 0x8c, 0xf1, 0xe3, 0xc2, 0xe5,
                        0xf2, 0x3a, 0x6b, 0xa0, 0xab, 0x90, 0xf4, 0xff])

    h = readavailable(io, d)

    @test tablesize(d) == 57
    @test d.table[lst+1] == (":authority"=>"www.example.com")

    @test length(h) == 4
    @test h[1] == (":method"=>"GET")
    @test h[2] == (":scheme"=>"http")
    @test h[3] == (":path"=>"/")
    @test h[4] == (":authority"=>"www.example.com")

    d
end

function spec_request_decode_huffman_2(d=spec_request_decode_huffman_1())
    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x82, 0x86, 0x84, 0xbe, 0x58, 0x86, 0xa8, 0xeb,
                        0x10, 0x64, 0x9c, 0xbf])

    h = readavailable(io, d)

    @test tablesize(d) == 110
    @test d.table[lst+1] == ("cache-control"=>"no-cache")
    @test d.table[lst+2] == (":authority"=>"www.example.com")

    @test length(h) == 5
    @test h[1] == (":method"=>"GET")
    @test h[2] == (":scheme"=>"http")
    @test h[3] == (":path"=>"/")
    @test h[4] == (":authority"=>"www.example.com")
    @test h[5] == ("cache-control"=>"no-cache")

    d
end

function spec_request_decode_huffman_3(d=spec_request_decode_huffman_2())
    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x82, 0x87, 0x85, 0xbf, 0x40, 0x88, 0x25, 0xa8, 0x49,
                        0xe9, 0x5b, 0xa9, 0x7d, 0x7f, 0x89, 0x25, 0xa8, 0x49,
                        0xe9, 0x5b, 0xb8, 0xe8, 0xb4, 0xbf])

    h = readavailable(io, d)

    @test tablesize(d) == 164
    @test d.table[lst+1] == ("custom-key"=>"custom-value")
    @test d.table[lst+2] == ("cache-control"=>"no-cache")
    @test d.table[lst+3] == (":authority"=>"www.example.com")

    @test length(h) == 5
    @test h[1] == (":method"=>"GET")
    @test h[2] == (":scheme"=>"https")
    @test h[3] == (":path"=>"/index.html")
    @test h[4] == (":authority"=>"www.example.com")
    @test h[5] == ("custom-key"=>"custom-value")

    d, io
end

function spec_response_decode_huffman_1()
    d = Decoder(max_table_size=256)

    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x48, 0x82, 0x64, 0x02, 0x58, 0x85, 0xae, 0xc3, 0x77,
                        0x1a, 0x4b, 0x61, 0x96, 0xd0, 0x7a, 0xbe, 0x94, 0x10,
                        0x54, 0xd4, 0x44, 0xa8, 0x20, 0x05, 0x95, 0x04, 0x0b,
                        0x81, 0x66, 0xe0, 0x82, 0xa6, 0x2d, 0x1b, 0xff, 0x6e,
                        0x91, 0x9d, 0x29, 0xad, 0x17, 0x18, 0x63, 0xc7, 0x8f,
                        0x0b, 0x97, 0xc8, 0xe9, 0xae, 0x82, 0xae, 0x43, 0xd3])

    h = readavailable(io, d)

    @test tablesize(d) == 222
    @test d.table[lst+1] == ("location"=>"https://www.example.com")
    @test d.table[lst+2] == ("date"=>"Mon, 21 Oct 2013 20:13:21 GMT")
    @test d.table[lst+3] == ("cache-control"=>"private")
    @test d.table[lst+4] == (":status"=>"302")

    @test length(h) == 4
    @test h[1] == (":status"=>"302")
    @test h[2] == ("cache-control"=>"private")
    @test h[3] == ("date"=>"Mon, 21 Oct 2013 20:13:21 GMT")
    @test h[4] == ("location"=>"https://www.example.com")

    d
end

function spec_response_decode_huffman_2(d=spec_response_decode_huffman_1())
    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x48, 0x83, 0x64, 0x0e, 0xff, 0xc1, 0xc0, 0xbf])

    h = readavailable(io, d)

    @test tablesize(d) == 222
    @test d.table[lst+1] == (":status"=>"307")
    @test d.table[lst+2] == ("location"=>"https://www.example.com")
    @test d.table[lst+3] == ("date"=>"Mon, 21 Oct 2013 20:13:21 GMT")
    @test d.table[lst+4] == ("cache-control"=>"private")

    @test length(h) == 4
    @test h[1] == (":status"=>"307")
    @test h[2] == ("cache-control"=>"private")
    @test h[3] == ("date"=>"Mon, 21 Oct 2013 20:13:21 GMT")
    @test h[4] == ("location"=>"https://www.example.com")

    d
end

function spec_response_decode_huffman_3(d=spec_response_decode_huffman_2())
    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x88, 0xc1, 0x61, 0x96, 0xd0, 0x7a, 0xbe, 0x94, 0x10,
                        0x54, 0xd4, 0x44, 0xa8, 0x20, 0x05, 0x95, 0x04, 0x0b,
                        0x81, 0x66, 0xe0, 0x84, 0xa6, 0x2d, 0x1b, 0xff, 0xc0,
                        0x5a, 0x83, 0x9b, 0xd9, 0xab, 0x77, 0xad, 0x94, 0xe7,
                        0x82, 0x1d, 0xd7, 0xf2, 0xe6, 0xc7, 0xb3, 0x35, 0xdf,
                        0xdf, 0xcd, 0x5b, 0x39, 0x60, 0xd5, 0xaf, 0x27, 0x08,
                        0x7f, 0x36, 0x72, 0xc1, 0xab, 0x27, 0x0f, 0xb5, 0x29,
                        0x1f, 0x95, 0x87, 0x31, 0x60, 0x65, 0xc0, 0x03, 0xed,
                        0x4e, 0xe5, 0xb1, 0x06, 0x3d, 0x50, 0x07])

    h = readavailable(io, d)

    @test tablesize(d) == 215
    @test d.table[lst+1] == ("set-cookie"=>"foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1")
    @test d.table[lst+2] == ("content-encoding"=>"gzip")
    @test d.table[lst+3] == ("date"=>"Mon, 21 Oct 2013 20:13:22 GMT")

    @test length(h) == 6
    @test h[1] == (":status"=>"200")
    @test h[2] == ("cache-control"=>"private")
    @test h[3] == ("date"=>"Mon, 21 Oct 2013 20:13:22 GMT")
    @test h[4] == ("location"=>"https://www.example.com")
    @test h[5] == ("content-encoding"=>"gzip")
    @test h[6] == ("set-cookie"=>"foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1")

    d
end


@testset "decode" begin

@testset "spec_request_decode" begin
    d = spec_request_decode_huffman_1()
    d = spec_request_decode_huffman_2(d)
    d = spec_request_decode_huffman_3(d)
end

@testset "spec_response_decode" begin
    d = spec_response_decode_huffman_1()
    d = spec_response_decode_huffman_2(d)
    d = spec_response_decode_huffman_3(d)
end

end
