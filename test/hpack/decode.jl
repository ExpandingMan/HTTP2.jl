using HTTP2, HTTP2.HPack
using Debugger, Test

using HTTP2.HPack: DecodeTable, Decoder, readentry, tablesize, entry!


function spec_header_field_decode_1()
    d = Decoder()

    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x40, 0x0a, 0x63, 0x75, 0x73, 0x74, 0x6f, 0x6d, 0x2d,
                        0x6b, 0x65, 0x79, 0x0d, 0x63, 0x75, 0x73, 0x74, 0x6f,
                        0x6d, 0x2d, 0x68, 0x65, 0x61, 0x64, 0x65, 0x72])
    r = bytesavailable(io)

    @test HPack.readentry(io, d) == ("custom-key"=>"custom-header", r)

    @test tablesize(d) == 55
    @test d.table[lst+1] == ("custom-key"=>"custom-header")

    d
end

function spec_header_field_decode_2()
    d = Decoder()

    io = IOBuffer(UInt8[0x04, 0x0c, 0x2f, 0x73, 0x61, 0x6d, 0x70, 0x6c,
                        0x65, 0x2f, 0x70, 0x61, 0x74, 0x68])
    r = bytesavailable(io)

    @test HPack.readentry(io, d) == (":path"=>"/sample/path", r)

    @test tablesize(d) == 0

    d
end

function spec_header_field_decode_3()
    d = Decoder()

    io = IOBuffer(UInt8[0x10, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72,
                        0x64, 0x06, 0x73, 0x65, 0x63, 0x72, 0x65, 0x74])
    r = bytesavailable(io)

    @test HPack.readentry(io, d) == ("password"=>"secret", r)

    @test tablesize(d) == 0

    d
end

function spec_header_field_decode_4()
    d = Decoder()

    io = IOBuffer(UInt8[0x82])

    @test HPack.readentry(io, d) == (":method"=>"GET", 1)

    @test tablesize(d) == 0

    d
end

function spec_request_decode_1()
    d = Decoder()

    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x82, 0x86, 0x84, 0x41, 0x0f, 0x77, 0x77, 0x77, 0x2e,
                        0x65, 0x78, 0x61, 0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x63,
                        0x6f, 0x6d])

    h = readavailable(io, d)

    @test tablesize(d) == 57
    @test d.table[lst+1] == (":authority"=>"www.example.com")

    @test length(h) == 4
    @test h[1] == (":method"=>"GET")
    @test h[2] == (":scheme"=>"http")
    @test h[3] == (":path"=>"/")
    @test h[4] == (":authority"=>"www.example.com")

    d
end

function spec_request_decode_2(d=spec_request_decode_1())
    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x82, 0x86, 0x84, 0xbe, 0x58, 0x08, 0x6e, 0x6f,
                        0x2d, 0x63, 0x61, 0x63, 0x68, 0x65])

    h = readavailable(io, d)

    @test tablesize(d) == 110
    @test d.table[lst+1] == ("cache-control"=>"no-cache")
    @test d.table[lst+2] == (":authority"=>"www.example.com")

    @test length(h) == 5
    @test h[1] == (":method"=>"GET")
    @test h[2] == (":scheme"=>"http")
    @test h[3] == (":path"=>"/")
    @test h[4] == (":authority"=>"www.example.com")
    @test h[5] == ("cache-control"=>"no-cache")

    d
end

function spec_request_decode_3(d=spec_request_decode_2())
    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x82, 0x87, 0x85, 0xbf, 0x40, 0x0a, 0x63, 0x75, 0x73,
                        0x74, 0x6f, 0x6d, 0x2d, 0x6b, 0x65, 0x79, 0x0c, 0x63,
                        0x75, 0x73, 0x74, 0x6f, 0x6d, 0x2d, 0x76, 0x61, 0x6c,
                        0x75, 0x65])

    h = readavailable(io, d)

    @test tablesize(d) == 164
    @test d.table[lst+1] == ("custom-key"=>"custom-value")
    @test d.table[lst+2] == ("cache-control"=>"no-cache")
    @test d.table[lst+3] == (":authority"=>"www.example.com")

    @test length(h) == 5
    @test h[1] == (":method"=>"GET")
    @test h[2] == (":scheme"=>"https")
    @test h[3] == (":path"=>"/index.html")
    @test h[4] == (":authority"=>"www.example.com")
    @test h[5] == ("custom-key"=>"custom-value")

    d, io
end

function spec_response_decode_1()
    d = Decoder(max_table_size=256)

    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x48, 0x03, 0x33, 0x30, 0x32, 0x58, 0x07, 0x70, 0x72,
                        0x69, 0x76, 0x61, 0x74, 0x65, 0x61, 0x1d, 0x4d, 0x6f,
                        0x6e, 0x2c, 0x20, 0x32, 0x31, 0x20, 0x4f, 0x63, 0x74,
                        0x20, 0x32, 0x30, 0x31, 0x33, 0x20, 0x32, 0x30, 0x3a,
                        0x31, 0x33, 0x3a, 0x32, 0x31, 0x20, 0x47, 0x4d, 0x54,
                        0x6e, 0x17, 0x68, 0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f,
                        0x2f, 0x77, 0x77, 0x77, 0x2e, 0x65, 0x78, 0x61, 0x6d,
                        0x70, 0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d])

    h = readavailable(io, d)

    @test tablesize(d) == 222
    @test d.table[lst+1] == ("location"=>"https://www.example.com")
    @test d.table[lst+2] == ("date"=>"Mon, 21 Oct 2013 20:13:21 GMT")
    @test d.table[lst+3] == ("cache-control"=>"private")
    @test d.table[lst+4] == (":status"=>"302")

    @test length(h) == 4
    @test h[1] == (":status"=>"302")
    @test h[2] == ("cache-control"=>"private")
    @test h[3] == ("date"=>"Mon, 21 Oct 2013 20:13:21 GMT")
    @test h[4] == ("location"=>"https://www.example.com")

    d
end

function spec_response_decode_2(d=spec_response_decode_1())
    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x48, 0x03, 0x33, 0x30, 0x37, 0xc1, 0xc0, 0xbf])

    h = readavailable(io, d)

    @test tablesize(d) == 222
    @test d.table[lst+1] == (":status"=>"307")
    @test d.table[lst+2] == ("location"=>"https://www.example.com")
    @test d.table[lst+3] == ("date"=>"Mon, 21 Oct 2013 20:13:21 GMT")
    @test d.table[lst+4] == ("cache-control"=>"private")

    @test length(h) == 4
    @test h[1] == (":status"=>"307")
    @test h[2] == ("cache-control"=>"private")
    @test h[3] == ("date"=>"Mon, 21 Oct 2013 20:13:21 GMT")
    @test h[4] == ("location"=>"https://www.example.com")

    d
end

function spec_response_decode_3(d=spec_response_decode_2())
    lst = length(d.table.stable)

    io = IOBuffer(UInt8[0x88, 0xc1, 0x61, 0x1d, 0x4d, 0x6f, 0x6e, 0x2c, 0x20,
                        0x32, 0x31, 0x20, 0x4f, 0x63, 0x74, 0x20, 0x32, 0x30,
                        0x31, 0x33, 0x20, 0x32, 0x30, 0x3a, 0x31, 0x33, 0x3a,
                        0x32, 0x32, 0x20, 0x47, 0x4d, 0x54, 0xc0, 0x5a, 0x04,
                        0x67, 0x7a, 0x69, 0x70, 0x77, 0x38, 0x66, 0x6f, 0x6f,
                        0x3d, 0x41, 0x53, 0x44, 0x4a, 0x4b, 0x48, 0x51, 0x4b,
                        0x42, 0x5a, 0x58, 0x4f, 0x51, 0x57, 0x45, 0x4f, 0x50,
                        0x49, 0x55, 0x41, 0x58, 0x51, 0x57, 0x45, 0x4f, 0x49,
                        0x55, 0x3b, 0x20, 0x6d, 0x61, 0x78, 0x2d, 0x61, 0x67,
                        0x65, 0x3d, 0x33, 0x36, 0x30, 0x30, 0x3b, 0x20, 0x76,
                        0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x3d, 0x31])

    h = readavailable(io, d)

    @test tablesize(d) == 215
    @test d.table[lst+1] == ("set-cookie"=>"foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1")
    @test d.table[lst+2] == ("content-encoding"=>"gzip")
    @test d.table[lst+3] == ("date"=>"Mon, 21 Oct 2013 20:13:22 GMT")

    @test length(h) == 6
    @test h[1] == (":status"=>"200")
    @test h[2] == ("cache-control"=>"private")
    @test h[3] == ("date"=>"Mon, 21 Oct 2013 20:13:22 GMT")
    @test h[4] == ("location"=>"https://www.example.com")
    @test h[5] == ("content-encoding"=>"gzip")
    @test h[6] == ("set-cookie"=>"foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1")

    d
end


@testset "decode_nohuffman" begin

@testset "decode_table" begin
    dt = DecodeTable()
    lst = length(dt.stable)

    entry!(dt, "bogus1"=>"bogus2")

    @test dt[lst+1] == ("bogus1"=>"bogus2")

    entry!(dt, "bogus3"=>"bogus4")

    @test dt[lst+1] == ("bogus3"=>"bogus4")
    @test dt[lst+2] == ("bogus1"=>"bogus2")

    ns = HPack.entrysize("bogus3"=>"bogus4")+1
    HPack.setmaxsize!(dt, ns)

    @test tablesize(dt) < ns
    @test dt.max_size == ns
    @test dt[lst+1] == ("bogus3"=>"bogus4")
    @test_throws BoundsError dt[lst+2]
end

@testset "spec_header_examples" begin
    spec_header_field_decode_1()
    spec_header_field_decode_2()
    spec_header_field_decode_3()
    spec_header_field_decode_4()
end

@testset "spec_request_decode" begin
    d = spec_request_decode_1()
    d = spec_request_decode_2(d)
    d = spec_request_decode_3(d)
end

@testset "spec_response_decode" begin
    d = spec_response_decode_1()
    d = spec_response_decode_2(d)
    d = spec_response_decode_3(d)
end

end
