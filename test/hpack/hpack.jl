using Test
using HTTP2


@testset "HPack" begin
    @testset "encode" begin
        include("encode.jl")
        include("encode_huffman.jl")
    end
    @testset "decode" begin
        include("decode.jl")
        include("decode_huffman.jl")
    end
end
