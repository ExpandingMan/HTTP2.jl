using Test
using HTTP2

using HTTP2.HPack: Encoder, HuffmanEncoder, entry!, tablesize

function spec_request_encode_huffman_1()
    e = Encoder(HuffmanEncoder())

    lst = length(e.table.stable)

    h = [":method"=>"GET",
         ":scheme"=>"http",
         ":path"=>"/",
         ":authority"=>"www.example.com",
        ]

    io = IOBuffer()
    write(io, e, h)

    @test tablesize(e) == 57
    @test e.table[":authority"=>"www.example.com"] == lst + 1

    @test take!(io) == UInt8[0x82, 0x86, 0x84, 0x41, 0x8c, 0xf1, 0xe3, 0xc2,
                             0xe5, 0xf2, 0x3a, 0x6b, 0xa0, 0xab, 0x90, 0xf4,
                             0xff]

    e
end

function spec_request_encode_huffman_2(e=spec_request_encode_huffman_1())
    lst = length(e.table.stable)

    h = [":method"=>"GET",
         ":scheme"=>"http",
         ":path"=>"/",
         ":authority"=>"www.example.com",
         "cache-control"=>"no-cache",
        ]

    io = IOBuffer()
    write(io, e, h)

    @test tablesize(e) == 110
    @test e.table["cache-control"=>"no-cache"] == lst + 1
    @test e.table[":authority"=>"www.example.com"] == lst + 2

    @test take!(io) == UInt8[0x82, 0x86, 0x84, 0xbe, 0x58, 0x86, 0xa8, 0xeb,
                             0x10, 0x64, 0x9c, 0xbf]

    e
end

function spec_request_encode_huffman_3(e=spec_request_encode_huffman_2())
    lst = length(e.table.stable)

    h = [":method"=>"GET",
         ":scheme"=>"https",
         ":path"=>"/index.html",
         ":authority"=>"www.example.com",
         "custom-key"=>"custom-value",
        ]

    io = IOBuffer()
    write(io, e, h)

    @test tablesize(e) == 164
    @test e.table["custom-key"=>"custom-value"] == lst + 1
    @test e.table["cache-control"=>"no-cache"] == lst + 2
    @test e.table[":authority"=>"www.example.com"] == lst + 3

    @test take!(io) == UInt8[0x82, 0x87, 0x85, 0xbf, 0x40, 0x88, 0x25, 0xa8,
                             0x49, 0xe9, 0x5b, 0xa9, 0x7d, 0x7f, 0x89, 0x25,
                             0xa8, 0x49, 0xe9, 0x5b, 0xb8, 0xe8, 0xb4, 0xbf]

    e
end

function spec_response_encode_huffman_1()
    e = Encoder(max_table_size=256)

    lst = length(e.table.stable)

    h = [":status"=>"302",
         "cache-control"=>"private",
         "date"=>"Mon, 21 Oct 2013 20:13:21 GMT",
         "location"=>"https://www.example.com",
        ]

    io = IOBuffer()
    write(io, e, h)

    @test tablesize(e) == 222
    @test e.table["location"=>"https://www.example.com"] == lst + 1
    @test e.table[h[3]] == lst + 2
    @test e.table["cache-control"=>"private"] == lst + 3
    @test e.table[":status"=>"302"] == lst + 4

    @test take!(io) == UInt8[0x48, 0x82, 0x64, 0x02, 0x58, 0x85, 0xae, 0xc3,
                             0x77, 0x1a, 0x4b, 0x61, 0x96, 0xd0, 0x7a, 0xbe,
                             0x94, 0x10, 0x54, 0xd4, 0x44, 0xa8, 0x20, 0x05,
                             0x95, 0x04, 0x0b, 0x81, 0x66, 0xe0, 0x82, 0xa6,
                             0x2d, 0x1b, 0xff, 0x6e, 0x91, 0x9d, 0x29, 0xad,
                             0x17, 0x18, 0x63, 0xc7, 0x8f, 0x0b, 0x97, 0xc8,
                             0xe9, 0xae, 0x82, 0xae, 0x43, 0xd3]

    e
end

function spec_response_encode_huffman_2(e=spec_response_example_huffman_1())
    lst = length(e.table.stable)

    h = [":status"=>"307",
         "cache-control"=>"private",
         "date"=>"Mon, 21 Oct 2013 20:13:21 GMT",
         "location"=>"https://www.example.com"
        ]

    io = IOBuffer()
    write(io, e, h)

    @test tablesize(e) == 222
    @test e.table[":status"=>"307"] == lst + 1
    @test e.table["location"=>"https://www.example.com"] == lst + 2
    @test e.table[h[3]] == lst + 3
    @test e.table["cache-control"=>"private"] == lst + 4

    @test take!(io) == UInt8[0x48, 0x83, 0x64, 0x0e, 0xff, 0xc1, 0xc0, 0xbf]

    e
end

function spec_response_encode_huffman_3(e=spec_response_example_huffman_2())
    lst = length(e.table.stable)

    h = [":status"=>"200",
         "cache-control"=>"private",
         "date"=>"Mon, 21 Oct 2013 20:13:22 GMT",
         "location"=>"https://www.example.com",
         "content-encoding"=>"gzip",
         "set-cookie"=>"foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1"
        ]

    io = IOBuffer()
    write(io, e, h)

    @test tablesize(e) == 215
    @test e.table[h[6]] == lst + 1
    @test e.table["content-encoding"=>"gzip"] == lst + 2
    @test e.table[h[3]] == lst + 3

    @test take!(io) == UInt8[0x88, 0xc1, 0x61, 0x96, 0xd0, 0x7a, 0xbe, 0x94,
                             0x10, 0x54, 0xd4, 0x44, 0xa8, 0x20, 0x05, 0x95,
                             0x04, 0x0b, 0x81, 0x66, 0xe0, 0x84, 0xa6, 0x2d,
                             0x1b, 0xff, 0xc0, 0x5a, 0x83, 0x9b, 0xd9, 0xab,
                             0x77, 0xad, 0x94, 0xe7, 0x82, 0x1d, 0xd7, 0xf2,
                             0xe6, 0xc7, 0xb3, 0x35, 0xdf, 0xdf, 0xcd, 0x5b,
                             0x39, 0x60, 0xd5, 0xaf, 0x27, 0x08, 0x7f, 0x36,
                             0x72, 0xc1, 0xab, 0x27, 0x0f, 0xb5, 0x29, 0x1f,
                             0x95, 0x87, 0x31, 0x60, 0x65, 0xc0, 0x03, 0xed,
                             0x4e, 0xe5, 0xb1, 0x06, 0x3d, 0x50, 0x07]

    e
end


@testset "encode_huffman" begin

@testset "spec_request_encodes" begin
    e = spec_request_encode_huffman_1()
    e = spec_request_encode_huffman_2(e)
    e = spec_request_encode_huffman_3(e)
end

@testset "spec_response_encodes" begin
    e = spec_response_encode_huffman_1()
    e = spec_response_encode_huffman_2(e)
    e = spec_response_encode_huffman_3(e)
end

end
