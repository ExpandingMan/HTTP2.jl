using Test
using HTTP2

using HTTP2.HPack: Encoder, IdentityStringEncoder, EncodeTable, entry!


function spec_header_field_encode_1()
    e = Encoder(IdentityStringEncoder())

    lst = length(e.table.stable)

    io = IOBuffer()
    write(io, e, "custom-key"=>"custom-header")

    @test HPack.tablesize(e) == 55
    @test e.table["custom-key"] == lst + 1
    @test e.table["custom-key"=>"custom-header"] == lst + 1

    @test take!(io) == UInt8[0x40, 0x0a, 0x63, 0x75, 0x73, 0x74, 0x6f, 0x6d,
                             0x2d, 0x6b, 0x65, 0x79, 0x0d, 0x63, 0x75, 0x73 ,
                             0x74, 0x6f, 0x6d, 0x2d, 0x68, 0x65, 0x61, 0x64,
                             0x65, 0x72]

    e
end

function spec_header_field_encode_2()
    e = Encoder(IdentityStringEncoder())

    io = IOBuffer()
    write(io, e, ":path"=>"/sample/path", literal_write=HPack.literalnoindex)

    @test HPack.tablesize(e) == 0
    @test isempty(e.table.dtable)

    @test take!(io) == UInt8[0x04, 0x0c, 0x2f, 0x73, 0x61, 0x6d, 0x70, 0x6c,
                             0x65, 0x2f, 0x70, 0x61, 0x74, 0x68]

    e
end

function spec_header_field_encode_3()
    e = Encoder(IdentityStringEncoder(), never_index=["password"])

    io = IOBuffer()
    write(io, e, "password"=>"secret")

    @test HPack.tablesize(e) == 0
    @test isempty(e.table.dtable)

    @test take!(io) == UInt8[0x10, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f,
                             0x72, 0x64, 0x06, 0x73, 0x65, 0x63, 0x72, 0x65,
                             0x74]

    e
end

function spec_header_field_encode_4()
    e = Encoder(IdentityStringEncoder())

    io = IOBuffer()
    write(io, e, ":method"=>"GET")

    @test HPack.tablesize(e) == 0
    @test isempty(e.table.dtable)

    @test take!(io) == UInt8[0x82]

    e
end

function spec_request_encode_1()
    e = Encoder(IdentityStringEncoder())

    lst = length(e.table.stable)

    h = [":method"=>"GET",
         ":scheme"=>"http",
         ":path"=>"/",
         ":authority"=>"www.example.com",
        ]

    io = IOBuffer()
    write(io, e, h)

    @test HPack.tablesize(e) == 57
    @test e.table[":authority"] == 1  # because it's in the static table
    @test e.table[":authority"=>"www.example.com"] == lst + 1

    @test take!(io) == UInt8[0x82, 0x86, 0x84, 0x41, 0x0f, 0x77, 0x77, 0x77,
                             0x2e, 0x65, 0x78, 0x61, 0x6d, 0x70, 0x6c, 0x65,
                             0x2e, 0x63, 0x6f, 0x6d]

    e
end

function spec_request_encode_2(e=spec_request_encode_1())
    lst = length(e.table.stable)

    h = [":method"=>"GET",
         ":scheme"=>"http",
         ":path"=>"/",
         ":authority"=>"www.example.com",
         "cache-control"=>"no-cache",
        ]

    io = IOBuffer()
    write(io, e, h)

    @test HPack.tablesize(e) == 110
    @test e.table["cache-control"] == 24  # this was in the static table
    @test e.table["cache-control"=>"no-cache"] == lst + 1
    @test e.table[":authority"=>"www.example.com"] == lst + 2

    @test take!(io) == UInt8[0x82, 0x86, 0x84, 0xbe, 0x58, 0x08, 0x6e, 0x6f,
                             0x2d, 0x63, 0x61, 0x63, 0x68, 0x65]

    e
end

function spec_request_encode_3(e=spec_request_encode_2())
    lst = length(e.table.stable)

    h = [":method"=>"GET",
         ":scheme"=>"https",
         ":path"=>"/index.html",
         ":authority"=>"www.example.com",
         "custom-key"=>"custom-value"
        ]

    io = IOBuffer()
    write(io, e, h)

    @test HPack.tablesize(e) == 164
    @test e.table["custom-key"] == lst + 1
    @test e.table["custom-key"=>"custom-value"] == lst + 1
    @test e.table["cache-control"=>"no-cache"] == lst + 2
    @test e.table[":authority"=>"www.example.com"] == lst + 3

    @test take!(io) == UInt8[0x82, 0x87, 0x85, 0xbf, 0x40, 0x0a, 0x63, 0x75,
                             0x73, 0x74, 0x6f, 0x6d, 0x2d, 0x6b, 0x65, 0x79,
                             0x0c, 0x63, 0x75, 0x73, 0x74, 0x6f, 0x6d, 0x2d,
                             0x76, 0x61, 0x6c, 0x75, 0x65]

    e
end

function spec_response_encode_1()
    e = Encoder(IdentityStringEncoder(), max_table_size=256)

    lst = length(e.table.stable)

    h = [":status"=>"302",
         "cache-control"=>"private",
         "date"=>"Mon, 21 Oct 2013 20:13:21 GMT",
         "location"=>"https://www.example.com",
        ]

    io = IOBuffer()
    write(io, e, h)

    @test HPack.tablesize(e) == 222
    @test e.table["location"=>"https://www.example.com"] == lst + 1
    @test e.table["date"=>"Mon, 21 Oct 2013 20:13:21 GMT"] == lst + 2
    @test e.table["cache-control"=>"private"] == lst + 3
    @test e.table[":status"=>"302"] == lst + 4

    @test take!(io) == UInt8[0x48, 0x03, 0x33, 0x30, 0x32, 0x58, 0x07, 0x70,
                             0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x61, 0x1d,
                             0x4d, 0x6f, 0x6e, 0x2c, 0x20, 0x32, 0x31, 0x20,
                             0x4f, 0x63, 0x74, 0x20, 0x32, 0x30, 0x31, 0x33,
                             0x20, 0x32, 0x30, 0x3a, 0x31, 0x33, 0x3a, 0x32,
                             0x31, 0x20, 0x47, 0x4d, 0x54, 0x6e, 0x17, 0x68,
                             0x74, 0x74, 0x70, 0x73, 0x3a, 0x2f, 0x2f, 0x77,
                             0x77, 0x77, 0x2e, 0x65, 0x78, 0x61, 0x6d, 0x70,
                             0x6c, 0x65, 0x2e, 0x63, 0x6f, 0x6d]

    e
end

function spec_response_encode_2(e=spec_response_encode_1())
    lst = length(e.table.stable)

    h = [":status"=>"307",
         "cache-control"=>"private",
         "date"=>"Mon, 21 Oct 2013 20:13:21 GMT",
         "location"=>"https://www.example.com",
        ]

    io = IOBuffer()
    write(io, e, h)

    @test HPack.tablesize(e) == 222
    @test e.table[":status"=>"307"] == lst + 1
    @test e.table["location"=>"https://www.example.com"] == lst + 2
    @test e.table["date"=>"Mon, 21 Oct 2013 20:13:21 GMT"] == lst + 3
    @test e.table["cache-control"=>"private"] == lst + 4

    @test take!(io) == UInt8[0x48, 0x03, 0x33, 0x30, 0x37, 0xc1, 0xc0, 0xbf]

    e
end

function spec_response_encode_3(e=spec_response_encode_2())
    lst = length(e.table.stable)

    h = [":status"=>"200",
         "cache-control"=>"private",
         "date"=>"Mon, 21 Oct 2013 20:13:22 GMT",
         "location"=>"https://www.example.com",
         "content-encoding"=>"gzip",
         "set-cookie"=>"foo=ASDJKHQKBZXOQWEOPIUAXQWEOIU; max-age=3600; version=1",
        ]

    io = IOBuffer()
    write(io, e, h)
    @test HPack.tablesize(e) == 215
    @test e.table[h[6]] == lst + 1
    @test e.table["content-encoding"=>"gzip"] == lst + 2
    @test e.table[h[3]] == lst + 3

    @test take!(io) == UInt8[0x88, 0xc1, 0x61, 0x1d, 0x4d, 0x6f, 0x6e, 0x2c,
                             0x20, 0x32, 0x31, 0x20, 0x4f, 0x63, 0x74, 0x20,
                             0x32, 0x30, 0x31, 0x33, 0x20, 0x32, 0x30, 0x3a,
                             0x31, 0x33, 0x3a, 0x32, 0x32, 0x20, 0x47, 0x4d,
                             0x54, 0xc0, 0x5a, 0x04, 0x67, 0x7a, 0x69, 0x70,
                             0x77, 0x38, 0x66, 0x6f, 0x6f, 0x3d, 0x41, 0x53,
                             0x44, 0x4a, 0x4b, 0x48, 0x51, 0x4b, 0x42, 0x5a,
                             0x58, 0x4f, 0x51, 0x57, 0x45, 0x4f, 0x50, 0x49,
                             0x55, 0x41, 0x58, 0x51, 0x57, 0x45, 0x4f, 0x49,
                             0x55, 0x3b, 0x20, 0x6d, 0x61, 0x78, 0x2d, 0x61,
                             0x67, 0x65, 0x3d, 0x33, 0x36, 0x30, 0x30, 0x3b,
                             0x20, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e,
                             0x3d, 0x31]

    e
end


@testset "encode_nohuffman" begin

@testset "encode_table" begin
    et = EncodeTable()
    lst = length(et.stable)

    entry!(et, "bogus1"=>"bogus2")

    @test et["bogus1"] == lst + 1
    @test et["bogus1"=>"bogus2"] == lst + 1

    entry!(et, "bogus3"=>"bogus4")

    @test et["bogus3"] == lst + 1
    @test et["bogus3"=>"bogus4"] == lst + 1
    @test et["bogus1"] == lst + 2
    @test et["bogus1"=>"bogus2"] == lst + 2

    ns = HPack.entrysize("bogus3"=>"bogus4")+1
    HPack.setmaxsize!(et, ns)

    @test HPack.tablesize(et) < ns
    @test et.max_size == ns
    # "bogus1"=>"bogus2" should have been evicted
    @test_throws KeyError et["bogus1"]
    @test et["bogus3"] == lst + 1
    @test et["bogus3"=>"bogus4"] == lst + 1
end

@testset "spec_header_examples" begin
    spec_header_field_encode_1()
    spec_header_field_encode_2()
    spec_header_field_encode_3()
    spec_header_field_encode_4()
end

@testset "spec_request_encode" begin
    e = spec_request_encode_1()
    e = spec_request_encode_2(e)
    e = spec_request_encode_3(e)
end

@testset "spec_response_encode" begin
    e = spec_response_encode_1()
    e = spec_response_encode_2(e)
    e = spec_response_encode_3(e)
end

end
