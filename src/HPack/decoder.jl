
# NOTE: the only way to know when all headers in the block have been read is by total bytes read
# can't store the limit in Decoder because it can be used for multiple frames
# so this must be an argument to read

"""
    Decoder

Data structure for reading compressed HPACK headers.

## Constructors
```julia
Decoder(;max_table_size=typemax(Int32))
```

## Arguments
- `max_table_size`: The maximum dynamic table size according to the definition given in the specification.

## Relevant Methods
- `read(io, d, n)`: Read at most `n` bytes from the stream `io` using the decoder `d`.

**WARNING:** the byte limit `n` is not a "hard" limit in that it is not checked on every byte read.  Instead, it merely
guarantees that no new header fields will be read past the `n`th byte.
"""
struct Decoder <: AbstractDecoder
    table::DecodeTable
    identity_string_decoder::IdentityStringDecoder
    huffman_string_decoder::HuffmanDecoder
end

function Decoder(;max_table_size::Integer=DEFAULT_MAX_TABLE_SIZE)
    Decoder(DecodeTable(max_size=max_table_size), IdentityStringDecoder(), HuffmanDecoder())
end

tablesize(d::Decoder) = tablesize(d.table)

getmaxsize(d::Decoder) = getmaxsize(d.table)

function readstring(io::IO, d::Decoder)
    b = read(io, UInt8)
    s, n = readstring(io, (b >> 7) == 0x01 ? d.huffman_string_decoder : d.identity_string_decoder, b)
    s, n+1
end

isprefixcode(f, b::UInt8) = (b >> prefix(f)) == (UInt8(f) >> prefix(f))

function index(d::Decoder, io::IO, b::UInt8)
    j, n = read_integer(io, b, prefix(index))
    d.table[j], n
end

function _literal_decode(f, d::Decoder, io::IO, b::UInt8)
    j, n₀ = read_integer(io, b, prefix(f))
    if j == 0
        k, n₁ = readstring(io, d)
        v, n₂ = readstring(io, d)
        k=>v, n₀+n₁+n₂
    else
        k = first(d.table[j])
        s, n₁ = readstring(io, d)
        k=>s, n₀+n₁
    end
end

function literal(d::Decoder, io::IO, b::UInt8)
    kv, n = _literal_decode(literal, d, io, b)
    entry!(d.table, kv)
    kv, n
end

literalnoindex(d::Decoder, io::IO, b::UInt8) = _literal_decode(literalnoindex, d, io, b)
literalneverindex(d::Decoder, io::IO, b::UInt8) = _literal_decode(literalneverindex, d, io, b)

setmaxsize!(d::Decoder, s::Integer) = setmaxsize!(d.table, s)

"""
    readentry(io, d::Decoder)

Read a single entry from the stream returning the entry and the number of bytes read.
"""
function readentry(io::IO, d::Decoder)
    b = read(io, UInt8)
    h, n = if isprefixcode(index, b)
        index(d, io, b)
    elseif isprefixcode(literal, b)
        literal(d, io, b)
    elseif isprefixcode(literalnoindex, b)
        literalnoindex(d, io, b)
    elseif isprefixcode(literalneverindex, b)
        literalneverindex(d, io, b)
    else
        error("tried to read headers with invalid first byte $b")
    end
    h, n+1
end

function Base.read(io::IO, d::Decoder, n::Integer)
    r = 0
    o = Vector{HeaderField}()
    while r < n
        h, r′ = readentry(io, d)
        push!(o, h)
        r += r′
    end
    o
end
Base.readavailable(io::IO, d::Decoder) = read(io, d, bytesavailable(io))

"""
    decode(d::Union{Decoder,Transcoder}, data::AbstractVector{UInt8})

Decode HPACK data from an array of bytes.
"""
decode(d::Decoder, data::AbstractVector{UInt8}) = readavailable(IOBuffer(data), d)
