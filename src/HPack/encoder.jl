
"""
    Encoder{ℰ<:StringEncoder}

Data structure for writing compressed HPACK headers.

## Constructors
```julia
Encoder(; never_index=Set{String}(), max_table_size=typemax(Int32))
```

## Arguments
- `never_index`: A set of header keys as strings which should never be indexed in the table in memory.
- `max_table_size`: The maximum dynamic table size.  The size definition is somewhat bizarre, see
    [here](https://httpwg.org/specs/rfc7541.html#calculating.table.size) for how it is defined.

## Relevant Methods
- `write(io, e, kv)`: Writes the header field or fields `kv` to the stream `io` using encoder `e`.
"""
struct Encoder{ℰ<:StringEncoder} <: AbstractEncoder
    table::EncodeTable
    string_encoder::ℰ
    neverindex::Set{String}  # we are doing this by key... not quite sure if that's ok
end

function Encoder(table::EncodeTable, se::StringEncoder; never_index=Set{String}())
    Encoder{typeof(se)}(table, se, Set{String}(never_index))
end

function Encoder(se::StringEncoder=HuffmanEncoder(); never_index=Set{String}(),
                 max_table_size::Integer=DEFAULT_MAX_TABLE_SIZE)
    Encoder(EncodeTable(max_size=max_table_size), se; never_index=Set{String}(never_index))
end

"""
    tablesize(ed)

Gives the current dynamic table size of the HPACK encoder or decoder `ed`.  The definition of the size is somewhat
bizarre, see [here](https://httpwg.org/specs/rfc7541.html#calculating.table.size) for the exact definition.
"""
tablesize(e::Encoder) = tablesize(e.table)

getmaxsize(e::Encoder) = getmaxsize(e.table)

@inline Base.getindex(e::Encoder, k) = e.table[k]

"""
    shouldneverindex(e::Encoder, kv::AbstractHeaderField)

Returns true iff the encoder `e` specifies that the header field should never be indexed.
"""
shouldneverindex(e::Encoder, kv::AbstractHeaderField) = first(kv) ∈ e.neverindex

"""
    index

Decompress a header field by referring to an indexed table entry.
"""
index(e::Encoder, io::IO, j::Integer) = write_integer(io, UInt8(index), prefix(index), j)

# this method is needed by literal
function literalnoindex(e::Encoder, b::UInt8, p::Integer, io::IO, j::Integer, v::AbstractString)
    o = write_integer(io, b, p, j)
    o + write(io, e.string_encoder, v)
end

"""
    literalnoindex

Decompress a literal header field, possibly by referring to the key as an indexed table entry, but without creating
any new table entries.
"""
function literalnoindex(e::Encoder, io::IO, j::Integer, v::AbstractString)
    literalnoindex(e, UInt8(literalnoindex), prefix(literalnoindex), io, j, v)
end

# this method is needed by literal
function literalnoindex(e::Encoder, b::UInt8, p::Integer, io::IO, kv::AbstractHeaderField)
    o = write(io, b)
    o += write(io, e.string_encoder, first(kv))
    o + write(io, e.string_encoder, last(kv))
end

function literalnoindex(e::Encoder, io::IO, kv::AbstractHeaderField)
    literalnoindex(e, UInt8(literalnoindex), prefix(literalnoindex), io, kv)
end

"""
    literal

Decompress a literal header field, possibly by referring to the key as an indexed table entry, but with the value
given literally.  Results in adding the header field as an entry to the dynamic table.
"""
function literal(e::Encoder, io::IO, kv::AbstractHeaderField)
    entry!(e.table, kv)
    literalnoindex(e, UInt8(literal), prefix(literal), io, kv)
end

# this method needed by `write`
function literal(e::Encoder, io::IO, j::Integer, kv::AbstractHeaderField)
    entry!(e.table, kv)
    literalnoindex(e, UInt8(literal), prefix(literal), io, j, last(kv))
end

# this method needed by `write`
literalnoindex(e::Encoder, io::IO, j::Integer, kv::AbstractHeaderField) = literalnoindex(e, io, j, last(kv))

"""
    literalneverindex

Decompress a literal header field, possibly by referring to the key as an indexed table entry, but forbidding the
value from ever being stored in the dynamic table.
"""
function literalneverindex(e::Encoder, io::IO, j::Integer, v::AbstractString)
    literalnoindex(e, UInt8(literalneverindex), prefix(literalneverindex), io, j, v)
end

function literalneverindex(e::Encoder, io::IO, kv::AbstractHeaderField)
    literalnoindex(e, UInt8(literalneverindex), prefix(literalneverindex), io, kv)
end

# leading bits are used to recognize the method in encoded data
Base.UInt8(::typeof(index)) = 0x80
Base.UInt8(::typeof(literal)) = 0x40
Base.UInt8(::typeof(literalneverindex)) = 0x10
Base.UInt8(::typeof(literalnoindex)) = 0x00

# length of the integer prefix in bytes indicating the method
prefix(::typeof(index)) = 7
prefix(::typeof(literal)) = 6
prefix(::typeof(literalneverindex)) = 4
prefix(::typeof(literalnoindex)) = 4

setmaxsize!(e::Encoder, s::Integer) = setmaxsize!(e.table, s)

function Base.write(io::IO, e::Encoder, kv::AbstractHeaderField; literal_write=literal)
    j = get(e.table, kv, missing)
    if ismissing(j)
        j = get(e.table, first(kv), missing)
        if ismissing(j)
            shouldneverindex(e, kv) ? literalneverindex(e, io, kv) : literal_write(e, io, kv)
        else
            literal_write(e, io, j, kv)
        end
    else
        index(e, io, j)
    end
end
Base.write(io::IO, e::Encoder, iter; literal_write=literal) = sum(write(io, e, kv; literal_write) for kv ∈ iter)

"""
    encode(e::Union{Encoder,Transcoder}, ϕ; literal_write=literal)

Encode header fields `ϕ` (either a `Pair{String,String}` or iterator thereof) using method `literal_write` for writing
literals returning a `Vector{UInt8}` of HPACK encoded data.
"""
function encode(e::Encoder, args...; kwargs...)
    io = IOBuffer()
    write(io, e, args...; kwargs...)
    take!(io)
end
