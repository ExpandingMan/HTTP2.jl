
"""
    huffmanlookuptree(v, ℓ)

Given huffman code `v` and word length `ℓ`, generate a nested dictionary allowing for efficient lookups of huffman words.

For the outer dict, the keys are the word lengths and the values are the inner dicts.  The inner dicts have the words as
keys with indices as values.

This is used to generate `HPack.HUFFMAN_LOOKUP_TREE` and under normal circumstances is never called at run time.
"""
function huffmanlookuptree(v::AbstractVector, ℓ::AbstractVector)
    o = Dict{Int,Dict{UInt,Int}}()
    for (j, (c, l)) ∈ enumerate(zip(v, ℓ))
        if haskey(o, l)
            o[l][c] = j-1
        else
            o[l] = Dict(c=>j-1)
        end
    end
    o
end

const HUFFMAN_CODE = [0x1ff8, 0x7fffd8, 0xfffffe2, 0xfffffe3, 0xfffffe4,
                      0xfffffe5, 0xfffffe6, 0xfffffe7, 0xfffffe8, 0xffffea,
                      0x3ffffffc, 0xfffffe9, 0xfffffea, 0x3ffffffd, 0xfffffeb,
                      0xfffffec, 0xfffffed, 0xfffffee, 0xfffffef, 0xffffff0,
                      0xffffff1, 0xffffff2, 0x3ffffffe, 0xffffff3, 0xffffff4,
                      0xffffff5, 0xffffff6, 0xffffff7, 0xffffff8, 0xffffff9,
                      0xffffffa, 0xffffffb, 0x14, 0x3f8, 0x3f9, 0xffa, 0x1ff9,
                      0x15, 0xf8, 0x7fa, 0x3fa, 0x3fb, 0xf9, 0x7fb, 0xfa, 0x16,
                      0x17, 0x18, 0x0, 0x1, 0x2, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
                      0x1e, 0x1f, 0x5c, 0xfb, 0x7ffc, 0x20, 0xffb, 0x3fc,
                      0x1ffa, 0x21, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63,
                      0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c,
                      0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0xfc, 0x73, 0xfd,
                      0x1ffb, 0x7fff0, 0x1ffc, 0x3ffc, 0x22, 0x7ffd, 0x3, 0x23,
                      0x4, 0x24, 0x5, 0x25, 0x26, 0x27, 0x6, 0x74, 0x75, 0x28,
                      0x29, 0x2a, 0x7, 0x2b, 0x76, 0x2c, 0x8, 0x9, 0x2d, 0x77,
                      0x78, 0x79, 0x7a, 0x7b, 0x7ffe, 0x7fc, 0x3ffd, 0x1ffd,
                      0xffffffc, 0xfffe6, 0x3fffd2, 0xfffe7, 0xfffe8, 0x3fffd3,
                      0x3fffd4, 0x3fffd5, 0x7fffd9, 0x3fffd6, 0x7fffda,
                      0x7fffdb, 0x7fffdc, 0x7fffdd, 0x7fffde, 0xffffeb,
                      0x7fffdf, 0xffffec, 0xffffed, 0x3fffd7, 0x7fffe0,
                      0xffffee, 0x7fffe1, 0x7fffe2, 0x7fffe3, 0x7fffe4,
                      0x1fffdc, 0x3fffd8, 0x7fffe5, 0x3fffd9, 0x7fffe6,
                      0x7fffe7, 0xffffef, 0x3fffda, 0x1fffdd, 0xfffe9,
                      0x3fffdb, 0x3fffdc, 0x7fffe8, 0x7fffe9, 0x1fffde,
                      0x7fffea, 0x3fffdd, 0x3fffde, 0xfffff0, 0x1fffdf,
                      0x3fffdf, 0x7fffeb, 0x7fffec, 0x1fffe0, 0x1fffe1,
                      0x3fffe0, 0x1fffe2, 0x7fffed, 0x3fffe1, 0x7fffee,
                      0x7fffef, 0xfffea, 0x3fffe2, 0x3fffe3, 0x3fffe4,
                      0x7ffff0, 0x3fffe5, 0x3fffe6, 0x7ffff1, 0x3ffffe0,
                      0x3ffffe1, 0xfffeb, 0x7fff1, 0x3fffe7, 0x7ffff2,
                      0x3fffe8, 0x1ffffec, 0x3ffffe2, 0x3ffffe3, 0x3ffffe4,
                      0x7ffffde, 0x7ffffdf, 0x3ffffe5, 0xfffff1, 0x1ffffed,
                      0x7fff2, 0x1fffe3, 0x3ffffe6, 0x7ffffe0, 0x7ffffe1,
                      0x3ffffe7, 0x7ffffe2, 0xfffff2, 0x1fffe4, 0x1fffe5,
                      0x3ffffe8, 0x3ffffe9, 0xffffffd, 0x7ffffe3, 0x7ffffe4,
                      0x7ffffe5, 0xfffec, 0xfffff3, 0xfffed, 0x1fffe6,
                      0x3fffe9, 0x1fffe7, 0x1fffe8, 0x7ffff3, 0x3fffea,
                      0x3fffeb, 0x1ffffee, 0x1ffffef, 0xfffff4, 0xfffff5,
                      0x3ffffea, 0x7ffff4, 0x3ffffeb, 0x7ffffe6, 0x3ffffec,
                      0x3ffffed, 0x7ffffe7, 0x7ffffe8, 0x7ffffe9, 0x7ffffea,
                      0x7ffffeb, 0xffffffe, 0x7ffffec, 0x7ffffed, 0x7ffffee,
                      0x7ffffef, 0x7fffff0, 0x3ffffee, 0x3fffffff]
const HUFFMAN_WORD_LENGTH = [13, 23, 28, 28, 28, 28, 28, 28, 28, 24, 30, 28,
                             28, 30, 28, 28, 28, 28, 28, 28, 28, 28, 30, 28, 28,
                             28, 28, 28, 28, 28, 28, 28, 6, 10, 10, 12, 13,
                             6, 8, 11, 10, 10, 8, 11, 8, 6, 6, 6, 5, 5, 5, 6,
                             6, 6, 6, 6, 6, 6, 7, 8, 15, 6, 12, 10, 13, 6, 7,
                             7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
                             7, 7, 7, 7, 8, 7, 8, 13, 19, 13, 14, 6, 15, 5, 6,
                             5, 6, 5, 6, 6, 6, 5, 7, 7, 6, 6, 6, 5, 6, 7, 6, 5,
                             5 , 6, 7, 7, 7, 7, 7, 15, 11, 14, 13, 28, 20, 22,
                             20, 20, 22, 22, 22, 23, 22, 23, 23, 23, 23, 23,
                             24, 23, 24, 24, 22, 23, 24, 23, 23, 23, 23, 21,
                             22, 23, 22, 23, 23, 24, 22, 21, 20, 22, 22, 23,
                             23, 21, 23, 22, 22, 24, 21, 22, 23, 23, 21, 21,
                             22, 21, 23, 22, 23, 23, 20, 22, 22, 22, 23, 22,
                             22, 23, 26, 26, 20, 19, 22, 23, 22, 25, 26, 26,
                             26, 27, 27, 26, 24, 25, 19, 21, 26, 27, 27, 26,
                             27, 24, 21, 21, 26, 26, 28, 27, 27, 27, 20, 24,
                             20, 21, 22, 21, 21, 23, 22, 22, 25, 25, 24, 24,
                             26, 23, 26, 27, 26, 26, 27, 27, 27, 27, 27, 28,
                             27, 27, 27, 27, 27, 26, 30]
const HUFFMAN_LOOKUP_TREE = huffmanlookuptree(HUFFMAN_CODE, HUFFMAN_WORD_LENGTH)
const HUFFMAN_MIN_WORD_LENGTH = minimum(keys(HUFFMAN_LOOKUP_TREE))
const HUFFMAN_MAX_WORD_LENGTH = maximum(keys(HUFFMAN_LOOKUP_TREE))


"""
    HuffmanEncoder()

Encodes strings according to a static Huffman code.  By default, this uses the Huffman code found in the
[HPACK specification](https://httpwg.org/specs/rfc7541.html).
"""
struct HuffmanEncoder <: StringEncoder
    code::Vector{UInt32}
    wordlength::Vector{Int}

    HuffmanEncoder(code=HUFFMAN_CODE, wordlength=HUFFMAN_WORD_LENGTH) = new(code, wordlength)
end

"""
    _bitamsk(T, α, β)

Create a bit mask of type `T` from bit `α` to bit `β` (inclusive) such that `1` corresponds to the *least* significant bit.
"""
function _bitmask(::Type{T}, α::Integer, β::Integer) where {T<:Integer}
    o = zero(T)
    for k ∈ α:β
        o += T(2)^(k-1)
    end
    o
end

"""
    _selectbits(n, ℓ, a, b)

Select bits `a` through `b` (inclusive) where the *MOST* significant bit is at index `1` and move them to the lowest
bits of an integer.  This strange choice of indexing scheme is used because it is useful in dealing with the huffman
encoding.
"""
function _selectbits(n::Integer, ℓ::Integer, a::Integer, b::Integer)
    (_bitmask(typeof(n), ℓ - b + 1, ℓ - a + 1) & n) >> (ℓ - b)
end

"""
    write_huffman_integer(io, b, p, n, ℓ)

Write the Huffman code word `n` of length `ℓ` bits to `io` starting with a `p` bit prefix on the byte `b` (that is,
`p` bits are available to be written on `b`).

Note that this requires `p > 1`, otherwise you will get a bogus answer.  No check for this is performed.
"""
function write_huffman_integer(io::IO, b::UInt8, p::Integer, n::Integer, ℓ::Integer)
    α, β = divrem(ℓ, 8)
    r = (8 + p - β) % 8  # prefix of next; i.e. unwritten bits on last byte
    if p > ℓ  # we only need to write current byte, but wait in case of more data
        b = UInt8(b) | UInt8(_selectbits(n, ℓ, 1, ℓ) << (p - ℓ))
        return 0, r, b
    else
        b = UInt8(b) | UInt8(_selectbits(n, ℓ, 1, p))
        o = write(io, b)
    end
    while true
        if ℓ - p ≤ 8  # we are on the last byte, may need to write more to it
            b = UInt8(_selectbits(n, ℓ, p+1, ℓ)) << r
            break
        else
            b = UInt8(_selectbits(n, ℓ, p+1, p+8))
            o += write(io, b)
            p += 8
        end
    end
    # since r will give prefix size for next, it r ∈ [1, 8]
    o, (r == 0 ? 8 : r), b
end

"""
    write_huffman_string(io, h, v)

Write the string `v` to `io` using the Huffman encoder `h`.  This does *NOT* include the encoding length integer.

If the final byte contains unused bits, these are filled with `1`s.  While the HPACK header does not explicitly indicate
this is required, there is no Huffman word for 7 `1`s and it can be seen in examples.  Alignment of the end of the code
to the final byte is required by the specification.
"""
function write_huffman_string(io::IO, h::HuffmanEncoder, v::AbstractVector{UInt8})
    idx = Int(v[1]) + 1
    o′, r, b = write_huffman_integer(io, 0x00, 8, h.code[idx], h.wordlength[idx])
    o = o′
    for i ∈ 2:length(v)
        idx = Int(v[i]) + 1
        o′, r, b = write_huffman_integer(io, b, r, h.code[idx], h.wordlength[idx])
        o += o′
    end
    # in this case we still need to write one more byte that didn't get written with last integer
    (r == 8 && b == 0x00) || write(io, UInt8(b) | _bitmask(UInt8, 1, r))
    o
end

"""
    write(io::IO, h::HuffmanEncoder, s)

Write the string `s` to `io` using the Huffman encoding of `h`.  This is for encoding strings in HPACK headers, and
can be used directly, that is, it writes the encoding length integer first, *with* the leading bit set to `1`
(this signals to HPACK implementations that the string is Huffman encoded).
"""
function Base.write(io::IO, h::HuffmanEncoder, v::AbstractVector{UInt8})
    ω = IOBuffer()
    write_huffman_string(ω, h, v)  # we have to write to a temporary buffer to figure out length of output
    b = take!(ω)
    write_integer(io, 0x80, 7, length(b)) + write(io, b)
end
Base.write(io::IO, h::HuffmanEncoder, s::AbstractString) = write(io, h, codeunits(s))


"""
    BytesWindow

Data structure representing a range of bits in an array of bytes.  The left boundary `w.j₁` gives the first bit
(according to 1-based indexing) and the right boundary `w.j₂` gives the last.  For example, the entirety of the 1-byte
array `v = [0xff]` can be described with `BytesWindow(v, 1, 8)`.

This struct is used for decoding HPACK Huffman code.

## Relevant Methods
- `UInt(w::BytesWindow)`: Converts the entire window into a single `UInt` object.  The length of the window must not
    exceed `8sizeof(UInt)` or bits will be lost.
- `extend(w::BytesWindow)`: Create a new `BytesWindow` with the latter index incremented by `1`.
"""
struct BytesWindow
    v::Vector{UInt8}
    j₁::Int  # start position
    j₂::Int  # end position
end

Base.length(w::BytesWindow) = max(0, w.j₂ - w.j₁ + 1)

extend(w::BytesWindow) = BytesWindow(w.v, w.j₁, w.j₂ + 1)

"""
    BitWindow

Data structure representing a range of bits in a *single* byte.  The left boundary `β.j₁` gives the first bit
(according to 1-based indexing) and the right boundary `w.j₂` gives the last.  For example, the least significant
4 bits of the byte `0xff` can be represented with `BitWindow(0xff, 4, 8)`.

## Relevant Methods
- `UInt8(β::BitWindow)`: Convert the bit window to a `UInt8`, that is, it removes bits outside of the specified range
    and moves them to the least significant end of the byte.
- `pushbits(o::UInt, β::BitWindow)`: Append the bits of the bit window to the integer `o`, moving exiting bits to
    more significant digits.
"""
struct BitWindow
    b::UInt8  # byte
    j₁::Int    # start position
    j₂::Int    # end position
end

Base.length(β::BitWindow) = max(0, β.j₂ - β.j₁ + 1)

# clear bits to the left of bit j (not inclusive)
function _clearleftbits(β::BitWindow)
    b = β.b << (β.j₁ - 1)
    b >> (β.j₁ - 1)
end

# align bits j₁:j₂, to the right
function Base.UInt8(β::BitWindow)
    b = _clearleftbits(β)
    b >> (8 - β.j₂)
end

# push the bits in the BitWindow to the integer o
function pushbits(o::UInt, β::BitWindow)
    b = UInt8(β)
    o = o << length(β)
    o ⊻ b
end

function Base.UInt(w::BytesWindow)
    o = zero(UInt)
    a₁, b₁ = fldmod1(w.j₁, 8)
    a₂, b₂ = fldmod1(w.j₂, 8)
    if a₁ == a₂
        β = BitWindow(w.v[a₁], b₁, b₂)
        o = pushbits(o, β)
    else
        β = BitWindow(w.v[a₁], b₁, 8)
        o = pushbits(o, β)
        for k ∈ (a₁+1):(a₂-1)
            o = pushbits(o, BitWindow(w.v[k], 1, 8))
        end
        β = BitWindow(w.v[a₂], 1, b₂)
        o = pushbits(o, β)
    end
    o
end


"""
    HuffmanDecoder()

Data structure for decoding HPACK Huffman encoded strings.  By default, this uses the static Huffman code given in the
[HPACK specification](https://httpwg.org/specs/rfc7541.html).

`HuffmanDecoder()[l, c]` gets the value of code word `c` of length `l`.

Strings can be read with `readstring(io, HuffmanDecoder())`.
"""
struct HuffmanDecoder <: StringDecoder
    tree::Dict{Int,Dict{UInt,Int}}
    min_word_length::Int
    max_word_length::Int

    function HuffmanDecoder(t::AbstractDict=HUFFMAN_LOOKUP_TREE, minwl::Integer=HUFFMAN_MIN_WORD_LENGTH,
                            maxwl::Integer=HUFFMAN_MAX_WORD_LENGTH)
        new(t, minwl, maxwl)
    end
end

# decode code word `o` with number of bits `l`
function Base.getindex(d::HuffmanDecoder, l::Integer, o::Unsigned)
    d1 = get(d.tree, l, missing)
    ismissing(d1) && return missing
    get(d1, o, missing)
end

"""
    readnextword(v::AbstractVector{UInt8}, j::Int, d::HuffmanDecoder)

Attempt to read the next word starting at bit `j` in a Huffman code given in `v`.  Returns a tuple of a found code word
and the next bit which has not yet been read.  If no valid code word can be found starting at `j` the first return
value is `nothing`.
"""
function readnextword(v::AbstractVector{UInt8}, j::Int, d::HuffmanDecoder)
    j + d.min_word_length - 1 > 8*length(v) && return (nothing, 0)  # nothing left to read
    w = BytesWindow(v, j, j+d.min_word_length-1)
    for i ∈ (d.min_word_length):(d.max_word_length)
        w.j₂ > 8*length(v) && return (nothing, 0)  # we hit the end of the buffer
        c = d[length(w), UInt(w)]
        ismissing(c) || return c, w.j₂+1
        w = extend(w)
    end
    nothing, 0
end

function readstring(v::AbstractVector{UInt8}, d::HuffmanDecoder)
    s = Vector{UInt8}()  # kind of sucks to be pushing this...
    j = 1
    while true
        c, j = readnextword(v, j, d)
        isnothing(c) && break
        push!(s, c)
    end
    String(s)
end

function readstring(io::IO, d::HuffmanDecoder, b::UInt8)
    ℓ, n = read_integer(io, b, 7)
    readstring(read(io, ℓ), d), ℓ+n
end
function readstring(io::IO, d::HuffmanDecoder)
    b = read(io, UInt8)
    s, n = readstring(io, d, b)
    s, n+1
end
