"""
    HPack

Header compression and decompression library for HTTP 2.

The specification can be found [here](https://httpwg.org/specs/rfc7541.html).
"""
module HPack

const HeaderField = Pair{String,String}
const AbstractHeaderField = Pair{<:AbstractString,<:AbstractString}
const EncodeDict = Dict{HeaderField,Int}
const EncodeKeyDict = Dict{String,Int}
const DecodeVector = Vector{HeaderField}

const DEFAULT_MAX_TABLE_SIZE = 2^12  # this is given in the spec

"""
    AbstractEncoder

Abstract type for all HPACK encoding data structures.
"""
abstract type AbstractEncoder end

"""
    AbstractDecoder

Abstract type for all HPACK decoding data structures.
"""
abstract type AbstractDecoder end

"""
    StringEncoder

Abstract type for encoding strings in HPACK.
"""
abstract type StringEncoder <: AbstractEncoder end

"""
    StringDecoder

Abstract type for decoding strings in HPAK.
"""
abstract type StringDecoder <: AbstractDecoder end


"""
    IdentityStringEncoder()

Singleton data structure used for trivially encoding strings in HPACK headers.  Strings encoded with this encoder appear
exactly as they do in memory and do not undergo any compression or any other type of transformation.
"""
struct IdentityStringEncoder <: StringEncoder end

"""
    IdentityStringDecoder()

Singleton data structure used for trivially decoding strings in HPACK headers.  This is simply used for moving bytes to
Julia `String` objects, no decompression nor any other type of transformation is applied.
"""
struct IdentityStringDecoder <: StringDecoder end


function Base.write(io::IO, e::IdentityStringEncoder, s::AbstractString)
    o = write_integer(io, 0x00, 7, ncodeunits(s))
    o + write(io, codeunits(s))
end

function Base.read(io::IO, d::IdentityStringDecoder, b::UInt8=read(io, UInt8))
    ℓ, _ = read_integer(io, b, 7)
    String(read(io, ℓ))
end

# returns bytes read
function readstring(io::IO, d::IdentityStringDecoder, b::UInt8)
    ℓ, n = read_integer(io, b, 7)
    s = String(read(io, ℓ))
    s, ncodeunits(s)+n
end
function readstring(io::IO, d::IdentityStringDecoder)
    b = read(io, UInt8)
    s, n = readstring(io, d, b)
    s, n+1
end

"""
    entrysize(kv::HeaderField)

The "size" of a header entry according to the HPACK specification.  This is defined as the sum of the size of the strings
plus, for some reason, 32, don't ask me why.
"""
entrysize(kv::HeaderField) = ncodeunits(first(kv)) + ncodeunits(last(kv)) + 32

"""
    _key_encode_table

Generate a table for looking up header keys from the original decoding table.  Wherever keys are repeated, the first one is
used.  While this ordering is not strictly required by the specification, we do this so that our implementation is
consistent with the examples provided in the specification and is more similar to other implementations
"""
function _key_encode_table(t::EncodeDict, d::DecodeVector)
    ot = Dict{String,Int}()
    for (k, v) ∈ t
        first(k) ∈ keys(ot) && continue
        ot[first(k)] = findfirst(p -> first(p) == first(k), d)
    end
    ot
end

@inline _bitencode(k, p, n) = (k & ~UInt8(2^p-1)) ⊻ UInt8(n)

"""
    write_integer

Write integer `n` to the stream with initial byte `b` and prefix length `p`.
"""
function write_integer(io::IO, b::UInt8, p::Integer, n::Integer)
    if n < 2^p - 1
        return write(io, _bitencode(b, p, n))
    end
    o = write(io, _bitencode(b, p, 2^p-1))
    n -= 2^p - 1
    while n ≥ 2^7
        o += write(io, UInt8((n % 2^7) + 2^7))
        n = fld(n, 2^7)
    end
    o += write(io, UInt8(n))
end

"""
    read_integer(io, b, p)

Read an integer from the stream.  The first byte `b` must be provided, along with the prefix length `p`.

Returns integer, number of bytes read.
"""
function read_integer(io::IO, b::UInt8, p::Integer)
    n = Int((2^p - 1) & b)
    n < 2^p - 1 && return n, 0
    M = 0
    r = 0
    while true
        b = read(io, UInt8)
        r += 1
        n += (b & (2^7 - 1)) * 2^M
        (b & 2^7) == 2^7 || break
        M += 7
    end
    n, r
end


include("huffmancode.jl")
include("statictable.jl")
include("encodetable.jl")
include("decodetable.jl")
include("encoder.jl")
include("decoder.jl")


struct Transcoder{ℰ<:StringEncoder}
    encoder::Encoder{ℰ}
    decoder::Decoder
end

"""
    Transcoder{ℰ<:StringEncoder}

Data structure for reading and writing of HPACK headers.  This is a simple convenience wrapper around `HPack.Encoder` and
`HPack.Decoder`.
"""
function Transcoder(se::StringEncoder=HuffmanEncoder(); never_index=Set{String}(),
                    max_encoder_table_size::Integer=DEFAULT_MAX_TABLE_SIZE,
                    max_decoder_table_size::Integer=DEFAULT_MAX_TABLE_SIZE)
    Transcoder{typeof(se)}(Encoder(se; max_table_size=max_encoder_table_size, never_index),
                           Decoder(max_table_size=max_decoder_table_size))
end

# both encoder and decoder in transcoder will be required to have same size
tablesize(t::Transcoder) = tablesize(t.encoder)
function setmaxsize!(t::Transcoder, s::Integer)
    setmaxsize!(t.encoder, s)
    setmaxsize!(t.decoder, s)
end

getmaxsize(t::Transcoder) = getmaxsize(t.encoder)

Encoder(t::Transcoder) = t.encoder
Decoder(t::Transcoder) = t.decoder

Base.read(io::IO, t::Transcoder, n::Integer) = read(io, Decoder(t), n)
Base.readavailable(io::IO, t::Transcoder) = read(io, Decoder(t))

Base.write(io::IO, t::Transcoder, kv::AbstractHeaderField; literal_write=literal) = write(io, Encoder(t), kv; literal_write)
Base.write(io::IO, t::Transcoder, iter; literal_write=literal) = write(io, Encoder(t), iter; literal_write)

decode(t::Transcoder, args...; kwargs...) = decode(Decoder(t), args...; kwargs...)
encode(t::Transcoder, args...; kwargs...) = encode(Encoder(t), args...; kwargs...)


end
