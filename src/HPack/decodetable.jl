
"""
    DecodeTable

Data structure containing an HPACK decoding table.  This includes the entries from the static table in the specification
as well as dynamic entries added when writing

`DecodeTable` is thread safe, retrieving and setting entries are atomic operations.

## Constructors
```julia
DecodeTable(;max_size=typemax(Int32))
```

## Arguments
- `max_size`: The maximum table size according to the definition given in the specification.

## Indexing
Header fields are retrieved using their index in the table, for example
```julia
dt = DecodeTable()

dt[1] # returns ":authority"=>""
```
"""
mutable struct DecodeTable
    stable::DecodeVector
    dtable::DecodeVector
    size::Int
    max_size::Int
    lock::ReentrantLock
end

function DecodeTable(dtable::DecodeVector=DecodeVector();
                     max_size::Integer=DEFAULT_MAX_TABLE_SIZE, static_table::DecodeVector=STATIC_TABLE)
    DecodeTable(static_table, dtable, 0, max_size, ReentrantLock())
end

tablesize(v::DecodeVector) = length(v) == 0 ? 0 : sum(entrysize.(v))

tablesize(dt::DecodeTable) = lock(() -> tablesize(dt.dtable), dt.lock)

getmaxsize(dt::DecodeTable) = lock(() -> getmaxsize(dt.dtable), dt.lock)

function _evictone!(dt::DecodeTable)
    kv = pop!(dt.dtable)
    dt.size -= entrysize(kv)
    dt
end

"""
    setmaxsize!(t, s::Integer)

Set the maximum size of the table `t`, evicting entries as necessary.
"""
function setmaxsize!(dt::DecodeTable, s::Integer)
    lock(dt.lock) do
        dt.max_size = s
        _evict!(dt)
    end
end

@inline function Base.getindex(dt::DecodeTable, j::Integer)::HeaderField
    lock(dt.lock) do
        j ≤ length(dt.stable) && return dt.stable[j]
        dt.dtable[mod1(j, length(dt.stable))]
    end
end

function entry!(dt::DecodeTable, kv::AbstractHeaderField)
    lock(dt.lock) do
        _entry_adjust!(dt, kv)  # this method is with the EncodeTable stuff
        pushfirst!(dt.dtable, kv)
    end
    kv
end
