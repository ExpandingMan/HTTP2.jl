
"""
    EncodeTable

Data structure containing an HPACK encoding table.  This includes the entries from the static table
[in the specification](https://httpwg.org/specs/rfc7541.html#static.table.definition) as well as dynamic entries
added when writing.

`EncodeTable` is thread safe, retrieving and setting entries are atomic operations.

## Constructors
```julia
EncodeTable(;max_size=typemax(Int32))
```

## Arguments
- `max_size`: The maximum table size according to the definition given in the specification.

## Indexing
`EncodeTable` objects can be indexed with either a header key or a header field.  The table index is returned.  For example:
```julia
et = EncodeTable()

et[":method"=>"POST"] # returns 3
et[":method"]  # returns 2, the index of the first table entry with the `":method"` key.

get(et, "bogus", missing)  # can use get for returning defaults as with dicts
```
"""
mutable struct EncodeTable
    stable::EncodeDict
    sktable::EncodeKeyDict
    dtable::EncodeDict
    dktable::EncodeKeyDict
    dtable_rev::DecodeVector  # dynamic table vector to keep track of oldest entry
    size::Int
    max_size::Int
    lock::ReentrantLock
end

getmaxsize(et::EncodeTable) = lock(() -> et.max_size, et.lock)

"""
    tablesize(et::EncodeTable)
    tablesize(dt::DecodeTable)

Computes the "table size" as defined in the HPACK specification.  This is computed by summing `entrysize` for all entries.
This counts *only* the size of the dynamic table, not the static table.
"""
tablesize(ed::EncodeDict) = length(ed) == 0 ? 0 : sum(entrysize(kv) for kv ∈ keys(ed))

tablesize(et::EncodeTable) = lock(() -> et.size, et.lock)

function _increment_dynamic_entries!(et)
    for (k,v) ∈ et.dtable
        et.dtable[k] = v + 1
    end
    for (k,v) ∈ et.dktable
        et.dktable[k] = v + 1
    end
end

# evict a single entry from the table
function _evictone!(et::EncodeTable)
    kv = pop!(et.dtable_rev)  # find oldest entry
    old_idx = et.dtable[kv]  # get what index it had
    delete!(et.dtable, kv)  # delete it from the dynamic lookup table
    kidx = get(et.dktable, first(kv), nothing)  # check if it's also in the key table
    kidx == old_idx && delete!(et.dktable, first(kv))  # if so, delete it from there
    et.size -= entrysize(kv)  # update table size
end

# this method also used by DecodeTable
function _evict!(t, δ::Integer=0)
    # table size is allowed to be 0, but in such cases there are no evictions
    while t.size > 0 && t.size + δ > t.max_size
        _evictone!(t)
    end
    t
end

"""
    evict!(et::EncodeTable, n=1)

Evict `n` entries from the HPACK encoding table `et`.
"""
evict!(et::EncodeTable, n::Integer=1) = foreach(i -> _evictone!(et), 1:n)

function setmaxsize!(et::EncodeTable, s::Integer)
    lock(et.lock) do
        et.max_size = s
        _evict!(et)
    end
end

function _pushfirstentry!(et::EncodeTable, kv::AbstractHeaderField)
    pushfirst!(et.dtable_rev, kv)
    idx = length(et.stable) + 1
    et.dtable[kv] = idx
    # should only need in key table if key doesn't already exist; may not always get lowest index from it then
    first(kv) ∈ keys(et.dktable) || (et.dktable[first(kv)] = idx)
end

# this function is also used by decodetable
function _entry_adjust!(t, kv::AbstractHeaderField)
    kvsize = entrysize(kv)
    ns = t.size + kvsize
    if ns ≤ t.max_size
        t.size = ns
    else
        _evict!(t, kvsize)
        t.size += kvsize
    end
end

"""
    entry!(et::EncodeTable, kv::AbstractHeaderField)

Insert the header field `kv` into the table.  As per the specification, it is always added as the first entry of the
dynamic table, evicting the last entries as needed to satisfy the maximum size constraint.
"""
function entry!(et::EncodeTable, kv::AbstractHeaderField)
    lock(et.lock) do
        _entry_adjust!(et, kv)
        _increment_dynamic_entries!(et)
        _pushfirstentry!(et, kv)
    end
    kv
end

function EncodeTable(dtable::DecodeVector=DecodeVector();
                     max_size::Integer=DEFAULT_MAX_TABLE_SIZE,
                     static_table::EncodeDict=STATIC_TABLE_REVERSE,
                     static_key_table::EncodeKeyDict=STATIC_TABLE_REVERSE_KEY)
    dict = EncodeDict(reverse(dtable) .=> ((length(dtable):-1:1) .+ length(static_table)))
    EncodeTable(static_table, static_key_table, dict, _key_encode_table(dict, dtable), dtable,
                tablesize(dict), max_size, ReentrantLock())
end


@inline function Base.get(et::EncodeTable, kv::AbstractHeaderField, d::D)::Union{Int,D} where {D}
    lock(et.lock) do
        j = get(et.stable, kv, missing)
        ismissing(j) || return j
        get(et.dtable, kv, d)
    end
end
@inline function Base.get(et::EncodeTable, k::AbstractString, d::D)::Union{Int,D} where {D}
    lock(et.lock) do
        j = get(et.sktable, k, missing)
        ismissing(j) || return j
        get(et.dktable, k, d)
    end
end

@inline function Base.getindex(et::EncodeTable, kv::AbstractHeaderField)
    o = get(et, kv, missing)
    ismissing(o) ? throw(KeyError(kv)) : o
end
@inline function Base.getindex(et::EncodeTable, k::AbstractString)
    o = get(et, k, missing)
    ismissing(o) ? throw(KeyError(k)) : o
end

