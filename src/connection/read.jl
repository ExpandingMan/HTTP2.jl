
function readheaders(χ::Connection, f::Frame)
    end_headers = getflag(f, :end_headers)
    end_stream = getflag(f, :end_stream)
    buf = copy(f.payload.headers_fragment)
    while !end_headers  # spec guarantees can only get from one stream, and we do not check
        f = read(χ.io, Frame)
        append!(buf, f.payload.headers_fragment)
        end_stream = end_stream || getflag(f, :end_stream)
        end_headers = getflag(f, :end_headers)
    end
    flags = [:end_headers]
    end_stream && push!(flags, :end_stream)
    Frame(streamid(f), UnpackedHeadersPayload(decode(HPack.Transcoder(χ), buf)), flags)
end

# TODO I think this is supposed to make continuation frames rather than HeadersPayload for subsequent
function headerframe(f::Frame{UnpackedHeadersPayload}, v::AbstractVector{UInt8}, i::Integer;
                     max_size::Integer=0)
    p = f.payload
    n = cld(length(v), max_size - p.pad_length)
    i > n && throw(ArgumentError("invalid headers payload window position $i of $n"))
    p = HeadersPayload(p.pad_length, p.stream_dependency, p.exclusive, p.weight,
                       v[_payload_index_range(n, max_size, p.pad_length, length(v), i)])
    flags = Symbol[ϕ for ϕ ∈ getflags(f) if ϕ ∉ (:end_headers, :end_stream)]
    if i == n
        push!(flags, :end_headers)
        getflag(f, :end_stream) && push!(flags, :end_stream)
    end
    Frame(streamid(f), p, flags)
end

function makeheaderframes(χ::Connection, f::Frame{UnpackedHeadersPayload})
    max_size = χ.state.max_frame_size
    v = encode(HPack.Transcoder(χ), f.payload.headers)
    map(i -> headerframe(f, v, i; max_size), 1:cld(length(v), max_size - f.payload.pad_length))
end

function legacyreadframe(χ::Connection)
    f = read(χ.io, Frame{LegacyPayload})
    @debug("$(show_short_string(χ)): read legacy frame", frame=f)
    f
end
function uninitreadframe(χ::Connection)
    f = read(χ.io, Frame{isserver(χ) ? ClientSettingsPayload : SettingsPayload})
    @debug("$(show_short_string(χ)): read initialization frame", frame=f)
    f
end
function readframe(χ::Connection)
    f = read(χ.io, Frame)
    f = isheaders(f) ? readheaders(χ, f) : f
    @debug("$(show_short_string(χ)): read frame", frame=f)
    f
end

function Base.read(χ::Connection, ::Type{Frame})
    𝓁 = χ.state.check_legacy ? checklegacy(χ.io) : false
    if 𝓁 || protocolstate(χ) ∈ (protocol_legacy, protocol_legacyupgrade)
        legacyreadframe(χ)
    elseif χ.state.protocol == protocol_uninit
        uninitreadframe(χ)
    else
        readframe(χ)
    end
end

# NOTE: if returns NullPayload, it means this is a protocol frame that should not be handled
function readiteration(𝒻, χ::Connection)
    f = if protocolstate(χ) == protocol_legacy
        legacyreadframe(χ)
    elseif protocolstate(χ) == protocol_legacyupgrade
        f′ = legacyreadframe(χ)
        upgradehandling(χ, f′) && return Frame(0, NullPayload())
        f′
    elseif !χ.state.init_received
        f′ = uninitreadframe(χ)  # will get settings frame that goes into acknowledge
        χ.state.init_received = true
        f′
    else
        readframe(χ)
    end
    acknowledge(χ, f) && return Frame(0, NullPayload())
    s = get!(χ, f)
    putincoming!(s, f)
    𝒻(s)
    f
end
readiteration(χ::Connection) = readiteration(_ -> nothing, χ)

# 𝒸 is condition to break
function readloop(𝒻, χ::Connection, 𝒸=(_ -> false))
    try
        @debug("$(show_short_string(χ)): read loop initiated")
        while !isnothing(χ.state.read_loop) && isopen(χ) && !eof(χ)
            f = readiteration(𝒻, χ)
            𝒸(f) && break
        end
        χ.state.read_loop = nothing
        @debug("$(show_short_string(χ)): read loop ended")
    catch e
        @error("read loop encountered error", exception=(e, catch_backtrace()), threadid())
        rethrow(e)
    end
end
readloop(χ::Connection, 𝒸=(_ -> false)) = readloop(_ -> nothing, χ, 𝒸)

readloop!(χ::Connection, t::Task) = (χ.state.read_loop = t)
readloop!(𝒻, χ::Connection, 𝒸=(_ -> false)) = readloop!(χ, @spawn readloop(𝒻, χ, 𝒸))
readloop!(χ::Connection, 𝒸=(_ -> false)) = readloop!(_ -> nothing, χ, 𝒸)

