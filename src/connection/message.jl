
function Message(::typeof(request), χ::Connection, method::AbstractString, path::AbstractString, body=UInt8[], h=HeaderField[])
    m = Message(request, joinpath(χ.host, path), method, body, h)
    upgradeheaders!(m, χ)
    m
end
function Message(::typeof(request), χ::Connection, method::AbstractString, body=UInt8[], h=HeaderField[])
    Message(request, χ, method, "", body, h)
end

function request(χ::Connection, method::AbstractString, path::AbstractString, body=UInt8[], h=HeaderField[])
    request(χ, Message(request, χ, method, path, body, h))
end
function request(χ::Connection, method::AbstractString, body=UInt8[], h=HeaderField[])
    request(χ, Message(request, χ, method, body, h))
end

function Base.read(𝒻, χ::Connection, ::Type{Message}, σ::Union{Nothing,Integer}=nothing)
    if isnothing(χ.state.read_loop)  # read until this particular message is read
        readloop!(𝒻, χ, (f -> (isnothing(σ) || streamid(f) == σ) && endofmessage(f)))
    end
    read(get!(χ, σ), Message)
end
Base.read(χ::Connection, ::Type{Message}, σ::Union{Nothing,Integer}=nothing) = read(_ -> nothing, χ, Message, σ)

# NOTE: this returns stream written to... not standard for write, unfortunately, but it's needed
function Base.write(χ::Connection, m::Message)
    s = get!(χ)
    write(s, m)
    if isnothing(χ.state.write_loop)  # iterate until this message is written
        writeloop!(χ, (f -> streamid(f) == streamid(s) && endofmessage(f)))
    end
    streamid(s)
end

function request(χ::Connection, m::Message)
    σ = write(χ, m)
    read(χ, Message, σ)
end

