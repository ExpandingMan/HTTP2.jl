
# these defaults are given in the specification, see [here](https://httpwg.org/specs/rfc7540.html#SettingValues)
const DEFAULT_MAX_CONCURRENT_STREAMS = 512
const DEFAULT_INITIAL_WINDOW_SIZE = 2^25  # this value used by curl
const DEFAULT_MAX_FRAME_SIZE = 2^14


include("state.jl")


struct Connection{ℐ<:IO} <: AbstractConnection
    host::URI
    io::ℐ

    is_server::Bool  # here rather than in state because it is immutable

    state::ConnectionState

    confluence::Confluence{Stream}
end


# TODO will need nicer ways of handling different IO types e.g. for https
function Connection(host::URI, io::IO=TCPSocket();
                    init::Bool=true, connect::Bool=true, run::Bool=true, is_server::Bool=false, kw...)
    χ = Connection{typeof(io)}(host, io, is_server, ConnectionState(;kw...), Confluence(Stream))
    init && settingsinit!(χ)  # this ensures preface is always the first thing in outgoing channel, unless legacy
    connect && Sockets.connect!(χ; run)
    χ
end
Connection(host::AbstractString, io::IO=TCPSocket(); kw...) = Connection(URI(host), io; kw...)

isserver(χ::Connection) = getfield(χ, :is_server)
isclient(χ::Connection) = !isserver(χ)

HPack.Transcoder(χ::Connection) = HPack.Transcoder(χ.state)

hostip(χ::Connection) = χ.host.host == "localhost" ? Sockets.localhost : first(Sockets.getalladdrinfo(χ.host.host))

makesettingsframe(χ::Connection; ack::Bool=false) = makesettingsframe(χ.state; ack)

function hostport(χ::Connection)
    if isempty(χ.host.port)
        if χ.host.scheme == "http"
            DEFAULT_HTTP_PORT
        elseif χ.host.scheme == "https"
            DEFAULT_HTTPS_PORT
        else
            throw(ArgumentError("tried to use host URI $(χ.host) with invalid scheme $(χ.host.scheme)"))
        end
    else
        parse(Int, χ.host.port)
    end
end

function _settingsinit!(χ::Connection, p::FramePayload)
    if protocolstate(χ) ∉ (protocol_legacyupgrade, protocol_legacy) && !χ.state.init_sent
        putoutgoing!(χ, Frame(0, p))
        χ.state.init_sent = true
    end
end

initial_settings_payload(χ::Connection) = isserver(χ) ? SettingsPayload(χ.state) : ClientSettingsPayload(χ.state)
initial_settings_payload(::typeof(empty), χ::Connection) = isserver(χ) ? SettingsPayload() : ClientSettingsPayload()

settingsinit!(χ::Connection)  = _settingsinit!(χ, initial_settings_payload(χ))
settingsinit!(::typeof(empty), χ::Connection)  = _settingsinit!(χ, initial_settings_payload(empty, χ))

function Sockets.connect!(χ::Connection; run::Bool=true)
    connect!(χ.io, hostip(χ), hostport(χ))
    run && run!(χ)
    χ
end

protocolstate(χ::Connection) = protocolstate(χ.state)
protocolstate!(χ::Connection, s::ProtocolState) = protocolstate!(χ.state, s)

# TODO this may have to check more stuff
Base.isopen(χ::Connection) = isopen(χ.io)

# TODO right now this is just a hack
Base.eof(χ::Connection) = false
Base.eof(χ::Connection{<:IOBuffer}) = eof(χ.io)

function Base.close(χ::Connection)
    close(χ.io)
    close(χ.confluence)
    χ.state.read_loop = nothing
    χ.state.write_loop = nothing
    nothing
end

function acknowledge_settings_frame(χ::Connection)
    # that this is empty is required by spec (https://httpwg.org/specs/rfc7540.html#SETTINGS)
    Frame(0, SettingsPayload(), [:ack])
end

function acknowledge(χ::Connection, p::SettingsPayload)
    for (k,v) ∈ p.settings
        setproperty!(χ.state, k, v)
    end
    putoutgoing!(χ.confluence, acknowledge_settings_frame(χ))
    protocolstate!(χ, protocol)
    @debug("$(show_short_string(χ)): received settings frame and sent acknowledgement")
    true
end
function acknowledge(χ::Connection, f::Frame{𝒫}) where {𝒫}
    # check to avoid run-time dispatch
    𝒫 <: Union{SettingsPayload,ClientSettingsPayload} || return false
    if getflag(f, :ack)
        @debug("$(show_short_string(χ)): received settings frame with ack flag")
        return true
    end
    acknowledge(χ, f.payload)
end

takeincoming!(χ::Connection, id::Integer) = takeincoming!(χ.confluence, id)
function putincoming!(𝒻, χ::Connection, f::AbstractFrame)
    # responding to a legacy stream after an upgrade is a special case
    if islegacyframe(f) && χ.state.protocol ∈ (protocol_uninit, protocol)
        putincoming!(χ.confluence, f)  # received frame gets inserted as usual
        get!(𝒻, χ.confluence, χ.is_server ? isserver : isclient)  # response on new stream
    else
        putincoming!(𝒻, χ.confluence, f)
    end
end
putincoming!(χ::Connection, f::Frame) = putincoming!(χ.confluence, f)

# this function will take the next frame to be written to the IO
takeoutgoing!(χ::Connection) = takeoutgoing!(χ.confluence)
putoutgoing!(χ::Connection, f::Frame) = putoutgoing!(χ.confluence, f)

legacystream!(χ::Connection) = legacystream!(χ.confluence)

function run!(𝒻, χ::Connection, 𝒸ʳ=(_ -> false), 𝒸ʷ=(_ -> false))
    readloop!(𝒻, χ, 𝒸ʳ)
    writeloop!(χ, 𝒸ʷ)
    χ
end
run!(χ::Connection, 𝒸ʳ=(_ -> false), 𝒸ʷ=(_ -> false)) = run!(_ -> nothing, χ, 𝒸ʳ, 𝒸ʷ)

Base.get!(χ::Connection, f::Frame) = get!(χ.confluence, f)
Base.get!(χ::Connection, id::Integer, s₀::StreamState=stream_idle) = get!(χ.confluence, id, s₀)
function Base.get!(χ::Connection, ::Nothing=nothing)
    if protocolstate(χ) ∈ (protocol_legacy, protocol_legacyupgrade)
        get!(χ.confluence, 1, stream_legacy)  # that legacy frames are considered in stream 1 is implied by protocol
    else
        get!(χ.confluence, χ.is_server ? isserver : isclient)
    end
end

#========================================================================================================
    \begin{upgrade mechanism}
========================================================================================================#
function upgradeheaders(χs::ConnectionState)
    ["connection"=>"Upgrade, HTTP2-Settings",
     "upgrade"=>"h2c",
     "http2-settings"=>base64encode(SettingsPayload(χs)),
     # NOTE: the below is the settings used by curl; may be useful for debugging
     #"http2-settings"=>"AAMAAABkAAQCAAAAAAIAAAAA",
    ]
end
upgradeheaders(χ::Connection) = upgradeheaders(χ.state)

function upgradeheaders!(m::Message, χ::Connection)
    χ.state.protocol == protocol_legacyupgrade && appendheaders!(m, upgradeheaders(χ))
end

is_upgrade_confirmation(::Frame) = false
is_upgrade_confirmation(f::Frame{<:LegacyPayload}) = getstatus(f.payload) == 101

function upgrade_settings(f::Frame{𝒫}) where {𝒫}
    # check to avoid run-time dispatch
    𝒫 <: LegacyPayload || return nothing
    s = nothing
    for ϕ ∈ ("HTTP2-Settings", "http2-settings", "Http2-Settings")
        s = getheader(f.payload, ϕ, nothing)
        isnothing(s) || break
    end
    isnothing(s) && return
    base64decode(SettingsPayload, s)
end

function upgradehandling(χ::Connection, f::Frame)
    p = upgrade_settings(f)
    if is_upgrade_confirmation(f)
        # upgrade confirmation frame not inserted into any stream
        protocolstate!(χ, protocol_uninit)
        settingsinit!(empty, χ)  # protocol specifies we must send another settings frame
        @debug("$(show_short_string(χ)): upgraded to HTTP2")
        true  # the above was special handling of the last frame so we must continue the read loop
    elseif !isnothing(p)
        s = get!(χ, 1)
        # the below serves as the settings ackowledgement
        write(χ.io, Frame(1, switching_protocols_payload()))  # we do this directly to ensure it finishes before next step
        setstate!(s, stream_open)  # stream 1 no longer in legacy state
        protocolstate!(χ, protocol_uninit)  # everything is done, so we are no longer in initialization
        settingsinit!(χ)  # send initial settings
        @debug("$(show_short_string(χ)): wrote upgrade confirmation message; upgraded to HTTP2")
        # after this the incoming frame with the upgrade request gets put in the appropriate new stream
        false
    else
        false
    end
end
#========================================================================================================
    \end{upgrade mechanism}
========================================================================================================#

#========================================================================================================
    \begin{includes}
========================================================================================================#
include("read.jl")
include("write.jl")
include("message.jl")
#========================================================================================================
    \end{includes}
========================================================================================================#

