
function Base.write(χ::Connection, f::Frame)
    o = write(χ.io, f)
    @debug("$(show_short_string(χ)): wrote frame", frame=f)
    o
end

function Base.write(χ::Connection, f::Frame{UnpackedHeadersPayload})
    o = sum(write(χ.io, f) for f ∈ makeheaderframes(χ, f))
    @debug("$(show_short_string(χ)): wrote frame", frame=f)
    o
end

function writeiteration(χ::Connection)
    f = takeoutgoing!(χ)
    write(χ, f)
    f
end

function writeloop(χ::Connection, 𝒸=(_ -> false))
    try
        @debug("$(show_short_string(χ)): write loop initiated")
        while !isnothing(χ.state.write_loop) && isopen(χ)
            f = writeiteration(χ)
            𝒸(f) && break
        end
        χ.state.write_loop = nothing
        @debug("$(show_short_string(χ)): write loop ended")
    catch e
        @error("write loop encountered error", exception=(e, catch_backtrace()), threadid())
        rethrow(e)
    end
end

writeloop!(χ::Connection, t::Task) = (χ.state.write_loop = t)
writeloop!(χ::Connection, 𝒸=(_ -> false)) = writeloop!(χ, @spawn writeloop(χ, 𝒸))

