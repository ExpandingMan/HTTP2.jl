
@enum ProtocolState begin
    protocol_legacy  # HTTP 1.1, no desired to upgrade
    protocol_legacyupgrade  # HTTP 1.1, requesting upgrade
    protocol_uninit  # HTTP 2, but no preface and settings from peer
    protocol  # normal operation of HTTP 2
end


mutable struct ConnectionState
    transcoder::HPack.Transcoder  # this has its own locks

    # TODO setting the loops probably need their own lock
    read_loop::Union{Nothing,Task}
    write_loop::Union{Nothing,Task}

    lock::ReentrantLock  # currently only used for protocol

    protocol::ProtocolState
    init_sent::Atomic{Bool}
    init_received::Atomic{Bool}

    check_legacy::Atomic{Bool}
    enable_push::Atomic{Bool}
    max_concurrent_streams::Atomic{Int}
    initial_window_size::Atomic{Int}
    max_frame_size::Atomic{Int}
end

"""
    _get_protocol_state(kwargs)

Get the initial protocol state from the keyword arguments passed to `ConnectionState`.  This defaults to legacy wanting
to upgrade.
"""
function _get_protocol_state(kwargs)
    if Base.get(kwargs, :prior_knowledge, false)
        protocol_uninit
    elseif Base.get(kwargs, :legacy, false)
        protocol_legacy
    else
        protocol_legacyupgrade
    end
end

function ConnectionState(;kwargs...)
    kwargs = Dict{Symbol,Any}(kwargs)
    mts = Base.get(kwargs, :header_table_size, HPack.DEFAULT_MAX_TABLE_SIZE)
    t = HPack.Transcoder(max_encoder_table_size=mts, max_decoder_table_size=mts)
    ConnectionState(t, nothing, nothing, ReentrantLock(),
                    _get_protocol_state(kwargs),
                    Atomic{Bool}(Base.get(kwargs, :init_sent, false)),
                    Atomic{Bool}(Base.get(kwargs, :init_received, false)),
                    Atomic{Bool}(Base.get(kwargs, :check_legacy, true)),
                    Atomic{Bool}(Base.get(kwargs, :enable_push, true)),
                    Atomic{Int}(Base.get(kwargs, :max_concurrent_streams, DEFAULT_MAX_CONCURRENT_STREAMS)),
                    Atomic{Int}(Base.get(kwargs, :initial_window_size, DEFAULT_INITIAL_WINDOW_SIZE)),
                    Atomic{Int}(Base.get(kwargs, :max_frame_size, DEFAULT_MAX_FRAME_SIZE)),
                   )
end

function SettingsPayload(χs::ConnectionState)
    SettingsPayload(:header_table_size=>HPack.getmaxsize(χs.transcoder),
                    :enable_push=>χs.enable_push,
                    :max_concurrent_streams=>χs.max_concurrent_streams,
                    :initial_window_size=>χs.initial_window_size,
                    :max_frame_size=>χs.max_frame_size,
                   )
end
ClientSettingsPayload(χs::ConnectionState) = ClientSettingsPayload(SettingsPayload(χs))

function makesettingsframe(χs::ConnectionState; ack::Bool=false)
    p = SettingsPayload(χs)
    fl = ack ? [:ack] : Symbol[]
    Frame(0, p, fl)
end

HPack.Transcoder(s::ConnectionState) = s.transcoder

function Base.setproperty!(χs::ConnectionState, ϕ::Symbol, v)
    if ϕ == :header_table_size
        HPack.setmaxsize!(χs.transcoder, v)
    elseif ϕ == :max_header_list_size
        v  # we ignore this for now; spec says only advisory
    elseif ϕ == :protocol
        protocolstate!(χs, v)
    elseif ϕ ∈ (:read_loop, :write_loop)
        setfield!(χs, ϕ, v)
    else
        getfield(χs, ϕ)[] = v
    end
end

function Base.getproperty(χs::ConnectionState, ϕ::Symbol)
    if ϕ == :transcoder
        getfield(χs, :transcoder)
    elseif ϕ == :header_table_size
        HPack.tablesize(χs.transcoder)
    elseif ϕ == :max_header_list_size
        0  # we ignore this for now; spec says only advisory
    elseif ϕ == :protocol
        protocolstate(χs)
    elseif ϕ ∈ (:read_loop, :write_loop)
        getfield(χs, ϕ)
    elseif ϕ ∈ (:lock,)
        getfield(χs, ϕ)
    else
        getfield(χs, ϕ)[]
    end
end

protocolstate(χs::ConnectionState) = lock(() -> getfield(χs, :protocol), getfield(χs, :lock))
protocolstate!(χs::ConnectionState, s::ProtocolState) = lock(() -> setfield!(χs, :protocol, s), getfield(χs, :lock))
