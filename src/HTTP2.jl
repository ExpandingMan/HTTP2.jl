module HTTP2

using Sockets, URIs, MbedTLS, Logging, Base64
using OrderedCollections, StringViews, ShowCases
using Base.Threads

using Base.Threads: @spawn, Condition

using Debugger  # TODO remove... some day...

using Sockets: connect!


const DEFAULT_HTTP_PORT = 80
const DEFAULT_HTTPS_PORT = 443

const DEFAULT_WINDOW_SIZE = 2^16 - 1  # given in the spec: https://httpwg.org/specs/rfc7540.html#InitialWindowSize


# logging
using Logging: Debug, Info
const Debug9 = LogLevel(-900)
const Debug8 = LogLevel(-800)
const Debug7 = LogLevel(-700)
const Debug6 = LogLevel(-600)
const Debug5 = LogLevel(-500)


abstract type AbstractStream end
abstract type AbstractConnection end
abstract type AbstractFrame end
abstract type FramePayload end
abstract type AbstractMessage end
abstract type AbstractServer end


export HPack


include("HPack/HPack.jl")
using .HPack: AbstractHeaderField, HeaderField, decode, encode


# forward declaration of some functions that get used as arguments
function request end
function response end


include("utils.jl")
include("frames.jl")
include("message.jl")
include("legacy.jl")
include("stream.jl")
include("connection/connection.jl")
include("server.jl")
include("show.jl")

# these are frame-specific constants given in the spec and must be defined after the structs
const _FRAME_TYPE_PAYLOAD_MAP = Dict{Int64,Type}(0=>DataPayload,
                                                 1=>HeadersPayload,
                                                 2=>PriorityPayload,
                                                 3=>RstStreamPayload,
                                                 4=>SettingsPayload,
                                                 5=>PushPromisePayload,
                                                 6=>PingPayload,
                                                 7=>GoAwayPayload,
                                                 8=>WindowUpdatePayload,
                                                 9=>ContinuationPayload,
                                                 # non-spec frame types
                                                 -1=>NullPayload,
                                                 -2=>ClientPrefacePayload,
                                                 -8=>LegacyPayload,
                                                 -64=>UnpackedHeadersPayload,
                                                 -997=>CorruptLegacyPayload,
                                                 -998=>CorruptPayloadWithHeader,
                                                 -999=>CorruptPayload,
                                                )
const _FLAGS_NAME_DICTS = Dict{Type,Dict{Symbol,Int}}(DataPayload=>Dict(:end_stream=>1, :padded=>4),
                                                      HeadersPayload=>Dict(:end_stream=>1, :end_headers=>3,
                                                                           :padded=>4, :priority=>6),
                                                      SettingsPayload=>Dict(:ack=>1),
                                                      ClientSettingsPayload=>Dict(:ack=>1),
                                                      PushPromisePayload=>Dict(:end_headers=>3, :padded=>4),
                                                      PingPayload=>Dict(:ack=>1),
                                                      ContinuationPayload=>Dict(:end_headers=>3),
                                                      # non-spec frame types
                                                      UnpackedHeadersPayload=>Dict(:end_stream=>1, :end_headers=>3,
                                                                                   :padded=>4, :priority=>6),
                                                     )

tryreadcheck(io::IO, 𝒯) = false
tryread(io::IO, 𝒯) = tryreadcheck(io, 𝒯) ? read(io, 𝒯) : nothing

# get the R-bit and stream ID from what is read in
function streamid(n::UInt32)
    rbit = Bool(n >> 31)
    ℓ = 0x7fffffff & n
    rbit, ℓ
end


end
