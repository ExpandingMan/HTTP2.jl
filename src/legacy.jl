
#========================================================================================================
    HTTP 1.1 Adaptation:

    Here we have a bunch of stuff for adapting legacy HTTP 1.1 stuff to work inside the new protocol.

    See [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages) for a description of
    HTTP 1.1 messages as the original spec is quite painful to read.
========================================================================================================#
const HTTP_LEGACY_VERSION_STRING = "HTTP/1.1"


# NOTE: this will store special headers as in HTTP2
struct LegacyPayload{𝒱} <: FramePayload
    headers::OrderedDict{String,String}
    body::𝒱
end

frametype(::Type{<:LegacyPayload}) = -8

function Base.codeunits(lp::LegacyPayload)
    io = IOBuffer()
    write(io, lp)
    take!(io)
end

Base.string(lp::LegacyPayload) = String(codeunits(lp))

function legacyheaderlength(p::AbstractHeaderField)
    isempty(p[1]) && return 0
    ncodeunits(p[1]) + ncodeunits(p[2]) + 4
end
legacyheaderlength(d) = sum(legacyheaderlength(kv) for kv ∈ d)

# this gives the current length, not the length as it would be determined by the protocol
Base.length(lp::LegacyPayload)  = ncodeunits(legacystartline(lp)) + legacyheaderlength(getheaders(lp)) + length(lp.body)

Base.IOBuffer(lp::LegacyPayload) = IOBuffer(lp.body, read=true, write=true)
Base.write(lp::LegacyPayload, x) = write(IOBuffer(lp), x)

function LegacyPayload(body::AbstractVector{UInt8}, headers=HeaderField[])
    # NOTE: this is different from Message constructor in that here we preserve header cases
    LegacyPayload{typeof(body)}(OrderedDict{String,String}(k=>v for (k,v) ∈ headers), body)
end
LegacyPayload(body::AbstractString, headers=HeaderField[]) = LegacyPayload(codeunits(body), headers)

Message(lp::LegacyPayload) = Message(lp.body, lp.headers)  # be sure to use correct method here

function _legacy_header_key(k::AbstractString)
    # seems to be no way out of special handling for this case
    if k == "http2-settings"
        "HTTP2-Settings"
    else
        join(uppercasefirst.(split(k, "-")), "-")
    end
end

function legacyheaders(m::Message; keep_pseudo::Bool=false)
    dct = OrderedDict{String,String}()
    for (k,v) ∈ m.headers
        keep_pseudo || (startswith(k, ":") && continue)
        dct[_legacy_header_key(k)] = v
    end
    # TODO this will require more elaborate handling at some point... not sure where we want it
    dct["Content-Length"] = string(length(m))
    dct
end

LegacyPayload(m::Message) = LegacyPayload(legacyheaders(m, keep_pseudo=true), m.body)

getheaders(lp::LegacyPayload) = lp.headers

getheader(lp::LegacyPayload, k::AbstractString) = lp.headers[k]
getheader(lp::LegacyPayload, k::AbstractString, default) = get(lp.headers, k, default)

function getstatus(lp::LegacyPayload)
    h = getheader(lp, ":status", nothing)
    isnothing(h) && return nothing
    parse(Int, h)
end

getheader!(lp::LegacyPayload, k::AbstractString, default::AbstractString) = get!(lp.headers, k, default)

headerkeys(lp::LegacyPayload) = keys(lp.headers)

isrequest(lp::LegacyPayload) = ":method" ∈ keys(lp.headers)
isresponse(lp::LegacyPayload) = ":status" ∈ keys(lp.headers)


function legacy_request_startline(io::IO, lp::LegacyPayload)
    meth = getheader(lp, ":method")
    if meth == "CONNECT"
        auth = getheader(lp, ":authority", nothing)
        if isnothing(auth)
            throw(ArgumentError("CONNECT method requires \":authority\" header field but none found"))
        end
        write(io, "CONNECT ")
        write(io, auth)
        write(io, " ")
        write(io, HTTP_LEGACY_VERSION_STRING)
        write(io, "\r\n")
    elseif meth == "OPTIONS"
        write(io, "OPTIONS * $HTTP_LEGACY_VERSION_STRING")
    else
        write(io, meth)
        write(io, " ")
        write(io, getheader(lp, ":path"))
        write(io, " $HTTP_LEGACY_VERSION_STRING")
        write(io, "\r\n")
    end
end

function legacy_response_startline(io::IO, lp::LegacyPayload)
    write(io, HTTP_LEGACY_VERSION_STRING)
    write(io, " ")
    write(io, getheader(lp, ":status"))
    # TODO not entirely sure this is permanent
    msg = getheader(lp, ":status-message", nothing)
    isnothing(msg) || write(io, " ") + write(io, msg)
    write(io, "\r\n")
end

function legacystartline(io::IO, lp::LegacyPayload)
    if isrequest(lp)
        legacy_request_startline(io, lp)
    elseif isresponse(lp)
        legacy_response_startline(io, lp)
    else
        throw(ArgumentError("cannot form valid HTTP 1.1 message from $io"))
    end
end
function legacystartline(lp::LegacyPayload)
    io = IOBuffer()
    legacystartline(io, lp)
    String(take!(io))
end

function legacyheaders(io::IO, lp::LegacyPayload)
    for (k,v) ∈ getheaders(lp)
        startswith(k, ":") && continue  # skip pseudo-headers for this
        write(io, k)
        write(io, ": ")
        write(io, v)
        write(io, "\r\n")
    end
    write(io, "\r\n")
end

legacybody(io::IO, lp::LegacyPayload) = write(io, lp.body)

function _legacystartline!(lp::LegacyPayload, io::IO)
    meth = readuntil(io, ' ')
    if startswith(meth, "HTTP")  # this is a response
        str = rstrip(readuntil(io, "\n"), '\r')
        lp.headers[":status"] = first(split(str, ' '))
    elseif all(c -> isascii(c) && isuppercase(c), meth)  # this is a request
        lp.headers[":method"] = meth
        meth == "OPTIONS" && return nothing
        r = readuntil(io, ' ')
        lp.headers[meth == "CONNECT" ? ":authority" : ":path"] = r
        readuntil(io, '\n')
    else
        throw(ArgumentError("got malformed HTTP 1.1 start line beginning with $meth"))
    end
    nothing
end
function legacystartline!(lp::LegacyPayload, io::IO)
    while true
        str = readline(io, keep=true)
        str ∈ ("\r\n", "\n") && continue
        return _legacystartline!(lp, IOBuffer(str))
    end
end

function legacyheaders!(lp::LegacyPayload, io::IO)
    while true
        l = rstrip(readuntil(io, "\n"), '\r')
        isempty(l) && return nothing
        k, v = split(l, ": ")
        lp.headers[k] = v
    end
end

#========================================================================================================
    TODO: body length stuff needs to be carefully overhauled when we support other transcoding schemes

    Also need a better way of supporting closed connections
========================================================================================================#

# compute body length according to spec, see https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.4
function legacybodylength(lp::LegacyPayload)
    s = getstatus(lp)
    !isnothing(s) && ((100 ≤ s < 200) || s == 204 || s == 304) && return 0
    getheader(lp, "Transfer-Encoding", "identity") ≠ "identity" && error("fuck, I didn't implement this yet")
    l = getheader(lp, "Content-Length", nothing)
    isnothing(l) || return parse(Int, l)
    # length can't be determined from above... TODO may need more here though
    nothing
end

function legacybody!(lp::LegacyPayload, io::IO)
    l = legacybodylength(lp)
    # if the length could not be determined, we read everything remaining
    v = isnothing(l) ? read(io) : read(io, l)
    write(lp, v)
end

# legacy frames always constitute a complete message... yes, even with chunking... I think
endofmessage(f::Frame{<:LegacyPayload}) = true

Message(f::Frame{<:LegacyPayload}) = Message(f.payload)

"""
    checklegacy(io)

Check if the next thing in the IO stream could plausibly be a legacy HTTP 1.1 message.  This is done by "peeking" at 4 bytes,
converting the first 3 to a string and checking if they are all uppercase ASCII characters.

This is experimental, but using it has certain nice properties, in particular that every frame can be handled exactly the
same way from the beginning of the connection
"""
function checklegacy(io::IO)
    v = [try  # obviously if this errors we don't have any kind of valid frame, but useful for other reasons
        peek(io, UInt32)
    catch e
        e isa EOFError && return false
        rethrow(e)
    end]
    str = String(reinterpret(UInt8, v)[1:3])  # check first 3 bytes
    # PRI is a special prefix for HTTP 2 preface
    all(s -> isascii(s) && isuppercase(s), str) && str ≠ "PRI" && return true
    false
end

Frame(lp::LegacyPayload) = Frame(1, lp, Symbol[])

legacyframe(m::Message) = Frame(LegacyPayload(m))

function update!(m::Message, f::Frame{<:LegacyPayload})
    for (k, v) ∈ getheaders(f.payload)
        m.headers[lowercase(k)] = v
    end
    write(m, f.payload.body)
    m
end

function readlegacypayload(io::IO)
    lp = LegacyPayload(UInt8[])
    legacystartline!(lp, io)
    legacyheaders!(lp, io)
    legacybody!(lp, io)
    lp
end
Base.read(io::IO, ::Type{LegacyPayload}) = readlegacypayload(io)
Base.read(io::IO, ::Type{Frame{<:LegacyPayload}}) = Frame(read(io, LegacyPayload))
# the below method is needed to resolve ambiguity with some other methods
Base.read(io::IO, ::Type{Frame{LegacyPayload}}) = Frame(read(io, LegacyPayload))

function Base.write(io::IO, lp::LegacyPayload)
    w = legacystartline(io, lp)
    w += legacyheaders(io, lp)
    w += legacybody(io, lp)
end
Base.write(io::IO, f::Frame{<:LegacyPayload}) = write(io, f.payload)

"""
    switching_protocols_payload()

Create a `LegacyPayload` containing the HTTP 1.1 response which is to be returned by servers to acknowledge
upgrade of the protocol from HTTP 1.1 to HTTP 2.0.
"""
function switching_protocols_payload()
    LegacyPayload(UInt8[],
                  [":status"=>"101", ":status-message"=>"Switching Protocols", "Connection"=>"Upgrade",
                   "Upgrade"=>"h2c", "Content-Length"=>"0"]
                 )
end

