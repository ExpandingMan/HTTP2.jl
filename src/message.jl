
const URIOrString = Union{URI,AbstractString}


struct Message{𝒱<:AbstractVector{UInt8}} <: AbstractMessage
    headers::OrderedDict{String,String}
    body::𝒱  # this needs to be flexible to allow for views
end

function Message(body::AbstractVector{UInt8}, headers=HeaderField[])
    Message(OrderedDict{String,String}([lowercase(k)=>v for (k,v) ∈ headers]), body)
end
Message(body::AbstractString, headers=HeaderField[]) = Message(codeunits(body), headers)

default_useragent() = "HTTP2.jl/$VERSION"

function hostheaderstring(url::URI)
    h = url.host
    isempty(url.port) ? h : h*":$(url.port)"
end

headerpathstring(url::URI) = isempty(url.path) ? "/" : url.path

appendheaders!(m::Message, hs) = appendheaders!(m.headers, hs)

function headers(url::URI, method::AbstractString, h=HeaderField[])
    hd = OrderedDict{String,String}(":method"=>method, ":path"=>headerpathstring(url))
    appendheaders!(hd, h)
    get!(hd, "accept", "*/*")
    get!(hd, "user-agent", default_useragent())
    get!(hd, "host", hostheaderstring(url))
    hd
end
headers(url::AbstractString, method::AbstractString, h=HeaderField[]) = headers(URI(url), method, h)

function headers(status, h=HeaderField[])
    hd = OrderedDict{String,String}(":status"=>string(status))
    appendheaders!(hd, h)
    hd
end

function Message(::typeof(request), url::URIOrString, method::AbstractString, body::AbstractVector{UInt8}, h=HeaderField[])
    Message(headers(url, method, h), body)
end
function Message(::typeof(request), url::URIOrString, method::AbstractString, body::AbstractString="", h=HeaderField[])
    Message(request, url, method, codeunits(body), h)
end

Message(::typeof(response), status, body::AbstractVector{UInt8}, h=HeaderField[]) = Message(headers(status, h), body)
Message(::typeof(response), status, body::AbstractString="", h=HeaderField[]) = Message(response, status, codeunits(body))

Base.IOBuffer(m::Message) = IOBuffer(m.body, read=true, write=true)

Base.length(m::Message) = length(m.body)

Base.write(m::Message, x) = write(IOBuffer(m), x)

isrequest(m::Message) = ":method" ∈ keys(m.headers)

isresponse(m::Message) = ":status" ∈ keys(m.headers)

getheaders(m::Message) = m.headers

getheader(m::Message, k::AbstractString) = m.headers[k]
getheader(m::Message, k::AbstractString, default) = get(m.headers, k, default)

function getstatus(m::Message)
    h = getheader(m, ":status", nothing)
    isnothing(h) && return nothing
    parse(Int, h)
end

getheader!(m::Message, k::AbstractString, default::AbstractString) = get!(m.headers, k, default)

headerkeys(m::Message) = keys(m.headers)

update!(m::Message, ::Frame) = m  # for protocol-related frames that are not part of the message
update!(m::Message, f::Frame{<:DataPayload}) = (write(m, f.payload.data); m)
function update!(m::Message, f::Frame{<:UnpackedHeadersPayload})
    for (k, v) ∈ f.payload.headers
        m.headers[k] = v
    end
    m
end
# method for LegacyPaylod is in legacy.jl

function Message(hook::Function, ch::Channel{Frame})
    m = Message(UInt8[])
    complete = false
    while isopen(ch)
        f = take!(ch)
        update!(m, f)
        if hook(f) || endofmessage(f)
            complete = true
            break
        end
    end
    complete || throw(ErrorException("channel closed before a complete message could be built"))
    m
end
Message(ch::Channel{Frame}) = Message(f -> false, ch)

function _payload_index_range(n::Integer, m::Integer, p::Integer, ℓ::Integer, i::Integer)
    a = (m - p)*(i - 1) + 1
    b = min(a + m - p - 1, ℓ)
    a:b
end

function DataPayload(m::Message, i::Integer=1; max_size::Integer=DEFAULT_WINDOW_SIZE, pad_length::Integer=0)
    n = cld(length(m), max_size - pad_length)  # number of payloads that will be needed
    i > n && throw(ArgumentError("invalid data payload window position $i of $n"))
    DataPayload(view(m.body, _payload_index_range(n, max_size, pad_length, length(m), i)); pad_length)
end

function makepayloads(::Type{DataPayload}, m::Message; max_size::Integer=DEFAULT_WINDOW_SIZE, pad_length::Integer=0)
    map(i -> DataPayload(m, i; max_size, pad_length), 1:cld(length(m), max_size - pad_length))
end


struct FrameIterator{ℳ<:AbstractMessage}
    m::ℳ

    stream_id::Int
    max_size::Int
    end_stream::Bool
    header_pad_length::Int
    body_pad_length::Int
    stream_dependency::Int
    stream_weight::Int
    stream_exclusive::Bool
end

function FrameIterator(m::Message, id::Integer; max_size::Integer=DEFAULT_WINDOW_SIZE, end_stream::Bool=true,
                       header_pad_length::Integer=0, body_pad_length::Integer=0, stream_dependency::Integer=0,
                       stream_weight::Integer=0, stream_exclusive::Bool=false)
    FrameIterator{typeof(m)}(m, id, max_size, end_stream, header_pad_length, body_pad_length, stream_dependency,
                             stream_weight, stream_exclusive)
end

nheaderframes(fi::FrameIterator) = 1
nbodyframes(fi::FrameIterator) = cld(length(fi.m), fi.max_size - fi.body_pad_length)

Base.length(fi::FrameIterator) = nheaderframes(fi) + nbodyframes(fi)

Base.eltype(::Type{FrameIterator}) = Frame

streamid(fi::FrameIterator) = fi.stream_id

function headersframe(fi::FrameIterator)
    p = UnpackedHeadersPayload(collect(fi.m.headers), pad_length=fi.header_pad_length, exclusive=fi.stream_exclusive,
                               weight=fi.stream_weight, stream_dependency=fi.stream_dependency)
    fl = [:end_headers]
    nbodyframes(fi) == 0 && push!(fl, :end_stream)
    Frame(streamid(fi), p, fl)
end

function Base.iterate(fi::FrameIterator, s::Integer=1)
    s == 1 && return (headersframe(fi), s+1)
    s > length(fi) && return nothing
    p = DataPayload(fi.m, s-1, max_size=fi.max_size, pad_length=fi.body_pad_length)
    fl = Symbol[]
    fi.body_pad_length > 0 && push!(fl, :padded)
    fi.end_stream && push!(fl, :end_stream)
    (Frame(streamid(fi), p, fl), s+1)
end

