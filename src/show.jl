
function _show_type(x; bold::Bool=false, show_module::Symbol=:never)
    ShowTypeOf(x; type_style=Style(:cyan, bold), show_module)
end

function Base.show(io::IO, h::FrameHeader)
    sep = Print(" = ")
    show(io, _show_type(x),
         ShowList(ShowKw(framepayload(h), :frame_type, sep),
                  ShowKw(streamid(h), :stream_id, sep),
                  ShowKw(Int(h.ℓ), :ℓ, sep),
                  ShowKw(getflags(h), :flags, sep),
                  new_lines=true,
                 )
        )
end

function Base.show(io::IO, f::Frame)
    sep = Print(" = ")
    p = ShowEntry(ShowIndents(f.payload, "    "), new_line=true, indent=repeat(" ", 8), delim=Print(""))
    show(io, _show_type(f),
         ShowList(ShowKw(streamid(f), :stream_id, sep),
                  ShowKw(Int(f.header.ℓ), :ℓ, sep),
                  ShowKw(getflags(f), :flags, sep),
                  ShowEntry(Styled(Print("Payload:"), :magenta), delim=ShowNothing(), new_line=true),
                  p,
                  new_lines=true,
                 )
        )
end

# default payload show
Base.show(io::IO, p::FramePayload) = show(io, _show_type(p), ShowProps(p))

function Base.show(io::IO, p::DataPayload; max_body_length::Integer=512)
    b = ShowLimit(String(copy(p.data)), limit=max_body_length)
    b = Styled(b, :yellow)
    show(io, _show_type(p), ShowList((b,)))
end

function Base.show(io::IO, p::HeadersPayload)
    show(io, _show_type(p),
         ShowList(ShowKw(length(p.headers_fragment), :fragment_length),
                  ShowKw(p.exclusive, :exclusive),
                  ShowKw(p.weight, :weight),
                  ShowKw(p.stream_dependency, :stream_dependency),
                 )
        )
end

function Base.show(io::IO, p::UnpackedHeadersPayload)
    h = collect(p.headers)
    h = [startswith(η[1], ":") ? Styled(η, :blue) : Styled(η, :normal) for η ∈ h]
    show(io, _show_type(p), ShowList(h))
end

function Base.show(io::IO, p::LegacyPayload; max_body_length::Integer=512)
    # TODO would be nice to limit this in a more intelligent way
    s = if isrequest(p) || isresponse(p)
        strb = IOBuffer()
        write(strb, p)
        take!(strb)
    else  # in this case we wound up with something uninitialized
        ""
    end
    b = ShowLimit(String(s), limit=max_body_length)
    b = Styled(b, :yellow)
    show(io, _show_type(p), ShowList((b,)))
end

function _showcase_headers(m::Message)
    h = collect(m.headers)
    h = [startswith(η[1], ":") ? Styled(η, :blue) : Styled(η, :normal) for η ∈ h]
    h = ShowList(h, brackets="", new_lines=true, indent=repeat(" ", 8))
    h = ShowEntry(h, new_line=false, delim=Print(""))
end

function _showcase_body(m::Message; max_body_length::Integer=512)
    b = ShowLimit(Print(String(copy(m.body))), limit=max_body_length)
    b = ShowString(b, indent=repeat(" ", 8))
    b = Styled(b, :yellow)
    b = ShowEntry(b, new_line=true, indent=repeat(" ", 8), delim=Print(""))
end

function Base.show(io::IO, m::Message; max_body_length::Integer=512)
    t = _show_type(m, bold=true)
    ℓ = ShowEntry(ShowKw(length(m.body), :ℓ), new_line=false)
    b = _showcase_body(m; max_body_length)
    h = _showcase_headers(m)
    p = ShowList(ℓ,
                 ShowEntry(Styled(Print("Headers:"), :magenta), new_line=true, delim=Print("")),
                 h, Print(repeat(" ", 4)),
                 ShowEntry(Styled(Print("Body:"), :magenta), delim=Print("")),
                 b, Print("\n")
                 ; delim=Print(""),
                )
    show(io, t, p)
end

function show_task(t::Task, name::Symbol=:Task)
    status = if istaskfailed(t)
        Styled(:failed, color=:red)
    elseif istaskdone(t)
        Styled(:done, color=:blue)
    elseif !istaskstarted(t)
        Styled(:ready, color=:normal)
    else
        Styled(:running, color=:green)
    end
    ShowCat(Print("$name"), Print("["), status, Print("]"))
end
function show_task(::Nothing, name::Symbol=:Task)
    ShowCat(Styled(Print("uninitialized"), color=:light_black), brackets="[]")
end

function Base.show(io::IO, et::HPack.EncodeTable)
    show(io, Styled(Print("HPack.EncodeTable"), :cyan),
         ShowList(ShowKw(et.size, :size),
                  ShowKw(et.max_size, :max_size),
                  ShowKw(ShowList(et.dtable_rev, brackets="[]"), :entries),
                 )
        )
end

function Base.show(io::IO, e::HPack.Encoder)
    show(io, Styled(Print("HPack.Encoder"), :cyan),
         ShowList(ShowKw(e.table.size, :table_size),
                  ShowKw(e.table.max_size, :max_table_size),
                  ShowKw(e.neverindex, :never_index),
                  ShowKw(ShowList(e.table.dtable_rev, brackets="[]"), :entries),
                 )
        )
end

function Base.show(io::IO, dt::HPack.DecodeTable)
    show(io, Styled(Print("HPack.DecodeTable"), :cyan),
         ShowList(ShowKw(dt.size, :size),
                  ShowKw(dt.max_size, :max_size),
                  ShowKw(ShowList(dt.dtable, brackets="[]"), :entries),
                 )
        )
end

function Base.show(io::IO, d::HPack.Decoder)
    show(io, Styled(Print("HPack.Decoder"), :cyan),
         ShowList(ShowKw(d.table.size, :table_size),
                  ShowKw(d.table.max_size, :max_table_size),
                  ShowKw(ShowList(d.table.dtable, brackets="[]"), :entries),
                 )
        )
end

function Base.show(io::IO, t::HPack.Transcoder)
    l = t.encoder.table.dtable_rev ∩ t.decoder.table.dtable
    show(io, Styled(Print("HPack.Transcoder"), :cyan),
         ShowList(ShowKw(t.encoder.table.size, :table_size),
                  ShowKw(t.encoder.table.max_size, :max_table_size),
                  ShowKw(ShowList(l, brackets="[]"), :entries),
                 )
        )
end

function Base.show(io::IO, χs::ConnectionState)
    show(io, _show_type(χs, bold=true),
         ShowList(χs.transcoder,
                  ShowKw(show_task(χs.read_loop), :read_loop, Print(": ")),
                  ShowKw(show_task(χs.write_loop), :write_loop, Print(": ")),
                  new_lines=true
                 )
        )
end

function _show_readiness(ch::Channel, name::AbstractString="Channel")
    r = isready(ch) ? ShowList((Styled(Print("ready"), :green),), brackets="[]") : ShowNothing()
    ShowCat(Print(name), r)
end

function Base.show(io::IO, s::Stream)
    c = if s.state ∈ (stream_reserved, stream_halfclosed)
        :light_red
    elseif s.state ∈ (stream_closed,)
        :red
    elseif s.state == stream_open
        :green
    else
        :normal
    end
    show(io, Styled(Print("HTTP2.Stream"), :cyan),
         ShowList(ShowKw(Styled(streamid(s), :magenta), :id),
                  Styled(s.state, c),
                  _show_readiness(s.incoming, "incoming"),
                  _show_readiness(s.outgoing, "outgoing")
                 )
        )
end

function _show_stream_list(γ::Confluence, indent=repeat(" ", 4), rb=Print(")"))
    lock(() -> ShowList(collect(values(γ.streams)); indent, new_lines=true, right_bracket=rb), γ)
end

Base.show(io::IO, γ::Confluence) = show(io, Styled(Print("HTTP2.Confluence"), :cyan, true), _show_stream_list(γ))

show_short(io::IO, χ::Connection) = print(io, "Connection[$(χ.host)]")
function show_short_string(χ::Connection)
    io = IOBuffer()
    show_short(io, χ)
    String(take!(io))
end

function Base.show(io::IO, χ::Connection)
    show(io, Styled(Print("HTTP2.Connection"), color=:cyan, bold=true),
         ShowList(ShowEntry(Styled(string(χ.host), :yellow), new_line=false),
                  ShowKw(show_task(χ.state.read_loop), :read_loop, Print(": ")),
                  ShowKw(show_task(χ.state.write_loop), :write_loop, Print(": ")),
                  ShowKw(_show_stream_list(χ.confluence, repeat(" ", 8), Print("    )")), :streams),
                  new_lines=true
                 )
        )
end
