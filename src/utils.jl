
"""
    boolvec(n)

Convert the integer `n` to a `Vector{Bool}` where each element corresponds to a bit in the integer.
"""
function boolvec(n::Integer)
    v = Vector{Bool}(undef, 8sizeof(n))
    for j ∈ 1:length(v)
        v[j] = ((2one(typeof(n)))^(j - 1) & n) >> (j - 1)
    end
    v
end

"""
    appendheaders!(hd, h)

Append the headers given by `h` to `hd`.  The header keys will automatically be converted to lowercase.
"""
function appendheaders!(hd, h=HeaderField[])
    for (k,v) ∈ h
        hd[lowercase(k)] = v
    end
    hd
end


struct IOReadRecorder{ℐ<:IO, ℛ<:IO} <: IO
    io::ℐ
    r::ℛ

    IOReadRecorder(io::IO, r::IO=IOBuffer()) = new{typeof(io),typeof(r)}(io, r)
end

function Base.read(io::IOReadRecorder, ::Type{UInt8})
    x = read(io.io, UInt8)
    write(io.r, x)
    x
end
function Base.unsafe_read(io::IOReadRecorder, p::Ptr{UInt8}, n::UInt)
    for i ∈ 1:n
        x = read(io, UInt8)
        unsafe_store!(p, x, i)
    end
    nothing
end

Base.write(io::IOReadRecorder, x::UInt8) = write(io.io, x)
Base.unsafe_write(io::IOReadRecorder, p::Ptr{UInt8}, n::UInt) = unsafe_write(io.io, p, n)

Base.eof(io::IOReadRecorder) = eof(io.io)
Base.seekstart(io::IOReadRecorder) = (seekstart(io.io); seekstart(io.r); io)

recorded(io::IOReadRecorder) = io.r
recorded(::typeof(read), io::IOReadRecorder) = recorded(io)


struct IOWriteRecorder{ℐ<:IO, 𝒲<:IO} <: IO
    io::ℐ
    w::𝒲

    IOWriteRecorder(io::IO, w::IO=IOBuffer()) = new{typeof(io),typeof(w)}(io, w)
end

Base.read(io::IOWriteRecorder, ::Type{UInt8}) = read(io.io, UInt8)
Base.unsafe_read(io::IOWriteRecorder, p::Ptr{UInt8}, n::UInt) = unsafe_read(io.io, p, n)

function Base.write(io::IOWriteRecorder, x::UInt8)
    write(io.w, x)
    write(io.io, x)
end
function Base.unsafe_write(io::IOWriteRecorder, p::Ptr{UInt8}, n::UInt)
    unsafe_write(io.w, p, n)
    unsafe_write(io.io, p, n)
end

Base.eof(io::IOWriteRecorder) = eof(io.io)
Base.seekstart(io::IOWriteRecorder) = (seekstart(io.io); seekstart(io.w); io)

recorded(io::IOWriteRecorder) = io.w
recorded(::typeof(write), io::IOWriteRecorder) = recorded(io)


struct IORecorder{ℐ<:IO, ℛ<:IO, 𝒲<:IO} <: IO
    r::IOReadRecorder{ℐ,ℛ}
    w::IOWriteRecorder{ℐ,𝒲}
end

function IORecorder(io::IO, r::IO=IOBuffer(), w::IO=IOBuffer())
    IORecorder{typeof(io),typeof(r),typeof(w)}(IOReadRecorder(io, r), IOWriteRecorder(io, w))
end

Base.read(io::IORecorder, ::Type{UInt8}) = read(io.r, UInt8)
Base.unsafe_read(io::IORecorder, p::Ptr{UInt8}, n::UInt) = unsafe_read(io.r, p, n)

Base.write(io::IORecorder, x::UInt8) = write(io.w, x)
Base.unsafe_write(io::IORecorder, p::Ptr{UInt8}, n::UInt) = unsafe_write(io.w, p, n)

Base.eof(io::IORecorder) = eof(io.r)
Base.seekstart(io::IORecorder) = (seekstart(io.r); seekstart(io.w.w); io)

recorded(::typeof(read), io::IORecorder) = recorded(io.r)
recorded(::typeof(write), io::IORecorder) = recorded(io.w)
