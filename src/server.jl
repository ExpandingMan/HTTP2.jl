
# TODO might want to do a version of this that stores references to all spawned tasks
response(𝒻) = s::AbstractStream -> @spawn respond(𝒻, s)

# TODO will of course need to listen for new connections as well
function serve(𝒻, port::Integer; kw...)
    tcps = listen(Sockets.localhost, port)
    io = accept(tcps)
    χ = Connection("http://localhost:$port", io; connect=false, is_server=true, kw...)
    run!(response(𝒻), χ)
    χ
end

# this version does not start its own loop
function serve(port::Integer; kw...)
    tcps = listen(Sockets.localhost, port)
    io = accept(tcps)
    Connection("http://localhost:$port", io; connect=false, is_server=true, kw...)
end
