
struct FrameHeader
    ℓ::UInt32
    type::Int64  # this type is way larger than needed so virtual types can be easily added
    flags::Vector{Bool}
    R::Bool
    stream_id::Int
end

# serialized length of the header in bytes
Base.length(h::FrameHeader) = 9

Base.:(==)(h::FrameHeader, h′::FrameHeader) = all(getfield(h, φ) == getfield(h′, φ) for φ ∈ 1:nfields(h))

streamid(h::FrameHeader) = h.stream_id

function FrameHeader(sid::Integer, p::FramePayload, flags::AbstractVector{Bool}; R::Bool=false)
    FrameHeader(length(p), frametype(typeof(p)), flags, R, sid)
end


struct Frame{𝒫<:FramePayload} <: AbstractFrame
    header::FrameHeader
    payload::𝒫
end

# serialized length of the entire frame in bytes
Base.length(f::Frame) = length(f.header) + length(f.payload)

Base.:(==)(f::Frame, f′::Frame) = all(getfield(f, φ) == getfield(f′, φ) for φ ∈ 1:nfields(f))

streamid(f::Frame) = streamid(f.header)

function Frame(sid::Integer, p::FramePayload, flags::AbstractVector{Symbol}=Symbol[]; R::Bool=false)
    h = FrameHeader(sid, p, makeflags(typeof(p), flags); R)
    Frame{typeof(p)}(h, p)
end

function flagsbyte(h::FrameHeader)
    b = 0x00
    for i ∈ 1:8
        b += (UInt8(h.flags[i]) << (i-1))
    end
    b
end

function writelength(io::IO, h::FrameHeader)
    # this rather bizarre code is to ensure it is written as a big-endian 24-bit integer
    write(io, reverse(reinterpret(UInt8, [h.ℓ])[1:3]))
end
writetype(io::IO, h::FrameHeader) = write(io, UInt8(h.type))

writeflags(io::IO, h::FrameHeader) = write(io, flagsbyte(h))

"""
    _writebool31(io, b, v)

A common pattern in HTTP 2 serialization is writing 32 bits where the first bit is reserved for a special boolean.
This writes that, with the bool `b` and value `v`.
"""
_writebool31(io::IO, b::Bool, v::Integer) = write(io, hton((b * 0x80000000) | UInt32(v)))

writestreamid(io::IO, h::FrameHeader) = _writebool31(io, h.R, UInt32(h.stream_id))

function Base.write(io::IO, h::FrameHeader)
    writelength(io, h) +
        writetype(io, h) +
        writeflags(io, h) +
        writestreamid(io, h)
end

# this rather bizarre code is to ensure we convert from a 24-bit big-endian integer
# also does checking for whether we have data in the buffer, since this is first read function called
function readframelength(io::IO)::Union{UInt32,Nothing}
    v = reverse(read(io, 3))
    isempty(v) && return nothing
    if length(v) ≠ 3
        error("tried to read a frame but only $(length(v)) bytes could be read")
    end
    only(reinterpret(UInt32, [v; 0x00]))
end

readframeflags(io::IO) = boolvec(read(io, UInt8))

readstreamid(io::IO) = streamid(ntoh(read(io, UInt32)))

Base.write(io::IO, f::Frame) = write(io, f.header) + write(io, f.payload, f.header)

# this method provided to resolve ambiguities
function readframeheader(io::IO)
    ℓ = readframelength(io)
    isnothing(ℓ) && return FrameHeader(NullPayload())
    type = read(io, UInt8)
    flags = readframeflags(io)
    R, id = readstreamid(io)
    FrameHeader(ℓ, type, flags, R, id)
end

Base.read(io::IO, ::Type{FrameHeader}) = readframeheader(io)

framepayload(h::FrameHeader) = framepayload(h.type)

Base.read(io::IO, ::Type{FramePayload}, h::FrameHeader) = read(io, framepayload(h), h)

# these methods are provided because they are in some cases needed to resolve method ambiguities
function readframe(io::IO, ::Type{𝒫}) where {𝒫<:FramePayload}
    h = read(io, FrameHeader)
    Frame(h, read(io, 𝒫, h))
end
function readframe(io::IO)
    h = read(io, FrameHeader)
    # TODO dynamic dispatch here is expensive, benchmark and change to a lookup
    Frame(h, read(io, FramePayload, h))
end

Base.read(io::IO, ::Type{Frame{𝒫}}) where {𝒫<:FramePayload} = readframe(io, 𝒫)
Base.read(io::IO, ::Type{Frame}) = readframe(io)

function registerframetype!(pm::AbstractDict, ::Type{𝒫}, id::Integer) where {𝒫<:FramePayload}
    if id ∈ keys(pm)
        throw(ArgumentError("frame type $id already exists for $(pm[id])"))
    end
    pm[id] = 𝒫
end
function registerframetype!(::Type{𝒫}, id::Integer) where {𝒫<:FramePayload}
    registerframetype!(_FRAME_TYPE_PAYLOAD_MAP, 𝒫, id)
end

framepayload(id::Integer) = _FRAME_TYPE_PAYLOAD_MAP[id]

flagsdict(::Type{𝒫}) where {𝒫<:FramePayload} = get(_FLAGS_NAME_DICTS, 𝒫, Dict{Symbol,Int}())
flagsdict(t::FramePayload) = flagsdict(typeof(t))

getflag(::Type{<:FramePayload}, h::FrameHeader, i::Integer) = h.flags[i]
getflag(::Type{𝒫}, h::FrameHeader, name::Symbol) where {𝒫<:FramePayload} = getflag(𝒫, h, flagsdict(𝒫)[name])
getflag(f::Frame{𝒫}, name) where {𝒫} = getflag(𝒫, f.header, name)

# if default is provided it won't matter if frame doesn't have that field
function getflag(f::Frame{𝒫}, name, default::Bool) where {𝒫}
    idx = Base.get(flagsdict(𝒫), name, nothing)
    isnothing(idx) && return default
    getflag(𝒫, f.header, idx)
end

endofstream(f::Frame) = getflag(f, :end_stream, false)

# this is the same as endofstream for everything except legacy messages
endofmessage(f::Frame) = endofstream(f)

function getflags(::Type{𝒫}, h::FrameHeader) where {𝒫}
    fl = Symbol[]
    for k ∈ keys(flagsdict(𝒫))
        getflag(𝒫, h, k) && push!(fl, k)
    end
    fl
end
getflags(h::FrameHeader) = getflags(framepayload(h), h)
getflags(f::Frame{𝒫}) where {𝒫} = getflags(𝒫, f.header)

isheaders(::FramePayload) = false

isheaders(f::Frame) = isheaders(f.payload)

"""
    makeflags(::Type{<:DataPayload}, f::AbstractVector{Symbol})

Make a flags `Vector{Bool}` of length 8 where the flags corresponding to names in `f` are set to true, else false.
"""
function makeflags(::Type{T}, f::AbstractVector{Symbol}) where {T<:FramePayload}
    o = falses(8)
    for 𝒻 ∈ f
        j = Base.get(flagsdict(T), 𝒻, missing)
        ismissing(j) && throw(ArgumentError("got unrecognized flag $𝒻 for $T"))
        o[j] = true
    end
    o
end

_writepadlength(io::IO, ℓ::Integer) = ℓ ≤ 0 ? 0 : write(io, UInt8(ℓ))

function _writepadding(io::IO, ℓ::Integer)
    o = 0
    for i ∈ 1:ℓ
        o += write(io, 0x00)
    end
    o
end


struct NullPayload <: FramePayload end

Base.length(::NullPayload) = 0

frametype(::Type{<:NullPayload}) = -1

Base.read(io::IO, ::Type{NullPayload}, h::FrameHeader) = NullPayload()

isnull(::Frame) = false
isnull(::Frame{<:NullPayload}) = true

FrameHeader(::NullPayload) = FrameHeader(0, -1, zeros(Bool,8), false, 0)

Frame(::NullPayload) = Frame(FrameHeader(NullPayload()), NullPayload())

# we deliberately have no `write` method for NullPayload


struct DataPayload{𝒱<:AbstractVector{UInt8}} <: FramePayload
    pad_length::Int
    data::𝒱  # this needs to be flexible to allow for views
end

DataPayload(data::AbstractVector{UInt8}; pad_length::Integer=0) = DataPayload{typeof(data)}(pad_length, data)

function Base.length(p::DataPayload)
    ℓ = length(p.data)
    p.pad_length > 0 ? (ℓ + p.pad_length + 1) : ℓ
end

frametype(::Type{<:DataPayload}) = 0

# TODO hopefully I'll find a better way of doing this
flagsdict(::Type{DataPayload{T}}) where {T} = _FLAGS_NAME_DICTS[DataPayload]

function Base.read(io::IO, ::Type{DataPayload}, h::FrameHeader)
    ℓ = h.ℓ
    pℓ = 0
    if getflag(DataPayload, h, :padded)
        pℓ = read(io, UInt8)
        ℓ -= 1
    end
    data = read(io, ℓ - pℓ)
    read(io, pℓ)  # we do this to advance position of the stream
    DataPayload{typeof(data)}(pℓ, data)
end

function Base.write(io::IO, p::DataPayload, h::FrameHeader)
    o = _writepadlength(io, p.pad_length)
    o += write(io, p.data)
    o += _writepadding(io, p.pad_length)
    o
end


struct HeadersPayload <: FramePayload
    pad_length::Int
    stream_dependency::UInt32  # 0x00 by default
    exclusive::Bool
    weight::Int
    headers_fragment::Vector{UInt8}
end

function HeadersPayload(buf::AbstractVector{UInt8};
                        pad_length::Integer=0, exclusive::Bool=false, weight::Integer=0,
                        stream_dependency::Integer=0)
    HeadersPayload(pad_length, stream_dependency, exclusive, weight, buf)
end

isheaders(::HeadersPayload) = true

function Base.length(p::HeadersPayload)
    ℓ = length(p.headers_fragment)
    p.pad_length > 0 && (ℓ += p.pad_length + 1)
    # we assume that the `:priority` flag is inferrable from this payload
    if p.exclusive || p.stream_dependency > 0
        ℓ += 5
    end
    ℓ
end

frametype(::Type{<:HeadersPayload}) = 1

function Base.read(io::IO, ::Type{HeadersPayload}, h::FrameHeader)
    ℓ = h.ℓ
    pℓ = 0
    sdep = UInt32(0x00)
    E = false
    w = 0
    if getflag(HeadersPayload, h, :padded)
        pℓ = read(io, UInt8)
        ℓ -= 1
    end
    if getflag(HeadersPayload, h, :priority)
        E, sdep = readstreamid(io)
        ℓ -= 4
        w = read(io, UInt8)
        ℓ -= 1
    end
    frag = read(io, ℓ - pℓ)
    read(io, pℓ)  # we do this to advance position of the stream
    HeadersPayload(pℓ, sdep, E, w, frag)
end

function Base.write(io::IO, p::HeadersPayload, h::FrameHeader)
    o = _writepadlength(io, p.pad_length)
    if getflag(HeadersPayload, h, :priority)
        o += _writebool31(io, p.exclusive, p.stream_dependency)
        o += write(io, UInt8(p.weight))
    end
    o += write(io, p.headers_fragment)
    o += _writepadding(io, p.pad_length)
end


# for storing headers after they ahve been decompressed
struct UnpackedHeadersPayload <: FramePayload
    pad_length::Int
    stream_dependency::Int
    exclusive::Bool
    weight::Int
    headers::Vector{HeaderField}
end

Base.length(::UnpackedHeadersPayload) = 0

function UnpackedHeadersPayload(hs::AbstractVector{<:AbstractHeaderField};
                                pad_length::Integer=0, exclusive::Bool=false, weight::Integer=0,
                                stream_dependency::Integer=0)
    UnpackedHeadersPayload(pad_length, stream_dependency, exclusive, weight, hs)
end

isheaders(::UnpackedHeadersPayload) = true

frametype(::Type{<:UnpackedHeadersPayload}) = -64

HPack.encode(t::HPack.Transcoder, p::UnpackedHeadersPayload) = HPack.encode(t, p.headers)
HPack.encode(t::HPack.Transcoder, f::Frame{UnpackedHeadersPayload}) = HPack.encode(t, f.payload)


struct PriorityPayload <: FramePayload
    stream_dependency::UInt32
    exclusive::Bool
    weight::Int
end

Base.length(p::PriorityPayload) = 5

frametype(::Type{<:PriorityPayload}) = 2

function Base.read(io::IO, ::Type{PriorityPayload}, h::FrameHeader)
    E, sdep = readstreamid(io)
    w = read(io, UInt8)
    PriorityPayload(sdep, E, w)
end

function Base.write(io::IO, p::PriorityPayload, h::FrameHeader)
    o = _writebool31(io, p.exlcusive, p.stream_dependency)
    o += write(io, UInt8(p.weight))
end


struct RstStreamPayload <: FramePayload
    error_code::Int
end

Base.length(p::RstStreamPayload) = 4

frametype(::Type{<:RstStreamPayload}) = 3

Base.read(io::IO, ::Type{RstStreamPayload}, h::FrameHeader) = RstStreamPayload(ntoh(read(io, UInt32)))

Base.write(io::IO, p::RstStreamPayload, h::FrameHeader) = write(io, hton(UInt32(p.error_code)))


struct SettingsPayload <: FramePayload
    settings::Dict{Symbol,Int}
end

SettingsPayload(args...) = SettingsPayload(Dict{Symbol,Int}(args...))

Frame(p::SettingsPayload) = Frame(0, p)

Base.length(p::SettingsPayload) = 6length(p.settings)

frametype(::Type{<:SettingsPayload}) = 4

# settings identifiers are fixed and defined in the spec: https://httpwg.org/specs/rfc7540.html#SettingValues
# at some point we may consider optionally including these in the payload to make it more extensible
const _SETTINGS_IDENTIFIERS = Dict{Symbol,UInt16}(:header_table_size=>0x0001,
                                                  :enable_push=>0x0002,
                                                  :max_concurrent_streams=>0x0003,
                                                  :initial_window_size=>0x0004,
                                                  :max_frame_size=>0x0005,
                                                  :max_header_list_size=>0x0006,
                                                 )
const _SETTINGS_IDENTIFIERS_INVERSE = Dict{UInt16,Symbol}(v=>k for (k,v) ∈ _SETTINGS_IDENTIFIERS)


function _readsetting(io::IO)
    id = ntoh(read(io, UInt16))
    v = ntoh(read(io, UInt32))
    k = get(_SETTINGS_IDENTIFIERS_INVERSE, id, nothing)
    isnothing(k) ? k : k=>Int(v)
end

function Base.read(io::IO, ::Type{SettingsPayload}, ℓ::Integer)
    s = Dict{Symbol,Int}()
    for i ∈ 1:fld(ℓ, 6)
        σ = _readsetting(io)
        # unknown settings must be ignored according to spec
        isnothing(σ) || push!(s, σ)
    end
    SettingsPayload(s)
end

Base.read(io::IO, ::Type{SettingsPayload}, h::FrameHeader) = read(io, SettingsPayload, h.ℓ)

function _writesetting(io::IO, p::Pair{Symbol,<:Integer})
    o = write(io, hton(_SETTINGS_IDENTIFIERS[p[1]]))
    o += write(io, hton(UInt32(p[2])))
end

function Base.write(io::IO, p::SettingsPayload)
    isempty(p.settings) && return 0
    sum(_writesetting(io, s) for s ∈ p.settings)
end

# this method needed by interface
Base.write(io::IO, p::SettingsPayload, ::FrameHeader) = write(io, p)

"""
    base64encode(p::SettingsPayload)

Returns a Base64 encoding of an HTTP2 settings payload.  This is required as a header field in HTTP upgrade requests.
"""
function Base64.base64encode(p::SettingsPayload)
    io = IOBuffer()
    write(io, p)
    base64encode(take!(io))
end

"""
    base64decode(::Type{SettingsPayload}, str)

Create an HTTP2.SettingsFrame object from a base64-encoded string.  These payloads are given as header fields in HTTP upgrade
requests.
"""
function Base64.base64decode(::Type{SettingsPayload}, str::AbstractString)
    v = base64decode(str)
    read(IOBuffer(v), SettingsPayload, length(v))
end


struct PushPromisePayload <: FramePayload
    pad_length::Int
    R::Bool
    stream_id::UInt32
    headers_fragment::Vector{UInt8}
end

isheaders(::PushPromisePayload) = true

function Base.length(p::PushPromisePayload)
    ℓ = length(p.headers_fragment)
    p.pad_length > 0 && (ℓ += p.pad_length + 1)
    ℓ + 4  # remaining 4 come from promised stream ID
end

frametype(::Type{<:PushPromisePayload}) = 5

function Base.read(io::IO, ::Type{PushPromisePayload}, h::FrameHeader)
    ℓ = h.ℓ
    pℓ = 0
    if getflag(PushPromisePayload, h, :padded)
        pℓ = read(io, UInt8)
        ℓ -= 1
    end
    R, psid = readstreamid(io)
    ℓ -= 4
    frag = read(io, ℓ - pℓ)
    read(io, pℓ)  # we do this to advance position of the stream
    PushPromisePayload(pℓ, R, psid, frag)
end


struct PingPayload <: FramePayload
    data::Vector{UInt8}
end

Base.length(p::PingPayload) = length(p.data)

frametype(::Type{<:PingPayload}) = 6

Base.read(io::IO, ::Type{PingPayload}, h::FrameHeader) = PingPayload(read(io, h.ℓ))

Base.write(io::IO, p::PingPayload, h::FrameHeader) = write(io, p.data)


struct GoAwayPayload <: FramePayload
    R::Bool
    last_stream_id::UInt32
    error_code::Int
    debug_data::Vector{UInt8}
end

Base.length(p::GoAwayPayload) = 8 + length(p.debug_data)

frametype(::Type{<:GoAwayPayload}) = 7

function Base.read(io::IO, ::Type{GoAwayPayload}, h::FrameHeader)
    ℓ = h.ℓ
    R, lsid = readstreamid(io)
    ℓ -= 4
    e = ntoh(read(io, UInt32))
    ℓ -= 4
    data = read(io, ℓ)
    GoAwayPayload(R, lsid, e, data)
end

function Base.write(io::IO, p::GoAwayPayload, h::FrameHeader)
    o = _writebool31(io, p.R, p.last_stream_id)
    o += write(io, hton(UInt32(p.error_code)))
    o += write(io, p.debug_data)
end


struct WindowUpdatePayload <: FramePayload
    R::Bool
    size_increment::Int
end

Base.length(p::WindowUpdatePayload) = 4

frametype(::Type{<:WindowUpdatePayload}) = 8

function Base.read(io::IO, ::Type{WindowUpdatePayload}, h::FrameHeader)
    R, s = readstreamid(io)
    WindowUpdatePayload(R, s)
end

Base.write(io::IO, p::WindowUpdatePayload, h::FrameHeader) = _writebool31(p.R, p.size_increment)


struct ContinuationPayload <: FramePayload
    headers_fragment::Vector{UInt8}
end

isheaders(::ContinuationPayload) = true

Base.length(p::ContinuationPayload) = length(p.headers_fragment)

frametype(::Type{<:ContinuationPayload}) = 9

Base.read(io::IO, ::Type{ContinuationPayload}, h::FrameHeader) = ContinuationPayload(read(io, h.ℓ))

Base.write(io::IO, p::ContinuationPayload, h::FrameHeader) = write(io, p.headers_fragment)


struct ClientPrefacePayload <: FramePayload end

client_preface_bytes() = b"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"
client_preface_string() = String(client_preface_bytes())

Base.length(::ClientPrefacePayload) = 24

frametype(::Type{<:ClientPrefacePayload}) = -2

function Base.read(io::IO, ::Type{ClientPrefacePayload})
    p = ClientPrefacePayload()
    v = read(io, length(p))
    if v ≠ client_preface_bytes()
        error("tried to read ClientPrefacePayload but got \"$(String(copy(v)))\"")
    end
    p
end
Base.read(io::IO, ::Type{Frame{ClientPrefacePayload}}) = read(io, ClientPrefacePayload)
Base.read(io::IO, ::Type{ClientPrefacePayload}, ::FrameHeader) = read(io, ClientPrefacePayload)

Base.write(io::IO, ::ClientPrefacePayload) = write(io, client_preface_bytes())
Base.write(io::IO, p::ClientPrefacePayload, ::FrameHeader) = write(io, p)
Base.write(io::IO, f::Frame{<:ClientPrefacePayload}) = write(io, f.payload)

Frame(p::ClientPrefacePayload) = Frame(0, p, Symbol[])

tryreadcheck(io::IO, ::Type{ClientPrefacePayload}) = reinterpret(UInt8, [peek(io, UInt32)]) == client_preface_bytes()[1:4]


# settings payload which is necessarily preceded by a preface
struct ClientSettingsPayload <: FramePayload
    settings::SettingsPayload
end

ClientSettingsPayload(args...) = ClientSettingsPayload(SettingsPayload(Dict{Symbol,Int}(args...)))

Base.length(p::ClientSettingsPayload) = length(ClientPrefacePayload()) + length(p.settings)

frametype(::Type{<:ClientSettingsPayload}) = 4

Frame(p::ClientSettingsPayload) = Frame(0, p)

function Base.read(io::IO, ::Type{Frame{ClientSettingsPayload}})
    read(io, ClientPrefacePayload)
    read(io, Frame{SettingsPayload})
end

function Base.write(io::IO, f::Frame{ClientSettingsPayload})
    write(io, ClientPrefacePayload()) + write(io, Frame(streamid(f), f.payload.settings))
end

#========================================================================================================
    \begin{robust reading}

    read frames from a stream and always return some kind of frame, even if it leaves the stream
    in a irrecoverably FUBAR state.  can be useful for debugging, but should *never* be used in
    the logic of "actual" programs.
========================================================================================================#
abstract type CorruptFramePayload <: FramePayload end

struct CorruptPayloadWithHeader <: CorruptFramePayload
    data::Vector{UInt8}
end

Base.length(p::CorruptPayloadWithHeader) = length(p.data)

frametype(::Type{<:CorruptPayloadWithHeader}) = -998

struct CorruptPayload <: CorruptFramePayload
    data::Vector{UInt8}
end

Base.length(p::CorruptPayload) = length(p.data)

frametype(::Type{<:CorruptPayload}) = -999

Frame(p::CorruptPayload) = Frame(-999, p)

struct CorruptLegacyPayload <: CorruptFramePayload
    data::Vector{UInt8}
end

Base.length(p::CorruptLegacyPayload) = length(p.data)

frametype(::Type{<:CorruptLegacyPayload}) = -997

Frame(p::CorruptLegacyPayload) = Frame(-999, p)

const CorruptFrameError = Union{EOFError,KeyError,InexactError,ArgumentError}

function _robustread(𝒻, ℊ, io::IO, errcheck=(e -> e isa CorruptFrameError))
    r = IOReadRecorder(io)
    try
        𝒻(r)
    catch e
        errcheck(e) || rethrow(e)
        # the below is normally supposed to be commented out
        #@warn("read a corrupt frame", exception=(e, catch_backtrace()))
        v = take!(recorded(read, r))
        ℊ(v)
    end
end

iscorrupt(::FramePayload) = false
iscorrupt(::CorruptFramePayload) = true
iscorrupt(f::Frame) = iscorrupt(f.payload)

# NOTE: for now a NullPayload indicates a termination
function robustread(io::IO, ::Type{Frame})
    if checklegacy(io)
        _robustread(io -> read(io, Frame{LegacyPayload}), v -> Frame(CorruptLegacyPayload(v)), io)
    end
    h = _robustread(io -> read(io, FrameHeader), v -> Frame(CorruptPayloadWithHeader(v)), io)
    h isa FrameHeader || return h
    v = _robustread(io -> read(io, h.ℓ), identity, io)  # ensure that we always read amount given by header
    io = IOBuffer(v)
    p = try
        read(io, FramePayload, h)
    catch e
        e isa CorruptFrameError || rethrow(e)
        CorruptPayload(v)
    end
    Frame(h, p)
end
#========================================================================================================
    \end{robust reading}
========================================================================================================#
