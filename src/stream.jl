
#========================================================================================================
    Special stream numbers:
    0: "master" stream.  Included in spec
========================================================================================================#

@enum StreamState begin
    stream_idle
    stream_reserved
    stream_open
    stream_halfclosed
    stream_closed
    stream_legacy
end

#========================================================================================================
    TODO: I'm a little dubious about whether the state graph in the spec is complete...
    sending headers with no data frames resultsin a `stream_halfclosed` state which is different
    from if it has data, which doesn't make a lot of sense.

    Think either this should close the stream or maybe such a stream needs to end with an extra frame?
========================================================================================================#

function transition_streamidle(f::Frame{𝒫}) where {𝒫}
    if 𝒫 <: PushPromisePayload
        stream_reserved
    elseif isheaders(f)
        stream_open
    else
        stream_idle
    end
end
function transition_streamreserved(f::Frame{𝒫}) where {𝒫}
    if 𝒫 <: RstStreamPayload
        stream_closed
    elseif isheaders(f)
        stream_halfclosed
    else
        stream_reserved
    end
end
function transition_streamopen(f::Frame{𝒫}) where {𝒫}
    if 𝒫 <: RstStreamPayload
        stream_closed
    elseif endofstream(f)
        stream_halfclosed
    else
        stream_open
    end
end
function transition_streamhalfclosed(f::Frame{𝒫}) where {𝒫}
    if 𝒫 <: RstStreamPayload || endofstream(f)
        stream_closed
    else
        stream_halfclosed
    end
end
transition_streamclosed(::Frame) = stream_closed

function transition(s::StreamState, f::Frame)
    if s == stream_idle
        transition_streamidle(f)
    elseif s == stream_reserved
        transition_streamreserved(f)
    elseif s == stream_open
        transition_streamopen(f)
    elseif s == stream_halfclosed
        transition_streamhalfclosed(f)
    elseif s == stream_closed
        transition_streamclosed(f)
    elseif s == stream_legacy
        stream_legacy  # legacy streams are always in the same state
    else
        throw(ArgumentError("tried to transition invalid stream state $s"))
    end
end

#========================================================================================================
Improtant Note:
    Only reading/writing `Message` can change the stream state.  This is
    important because the loops can always take buffered frames out of the
    stream and output over TCP regardless of stream state.  It is therefore
    safe to "close" the stream by writing a closing message response as it does
    not prevent data from being written out.
========================================================================================================#

struct Stream <: AbstractStream
    id::Int  # this is larger than the spec to allow for virtual streams

    # TODO changing from legacy state may require an additional lock for the channels
    state_lock::ReentrantLock
    state::Ref{StreamState}

    incoming::Channel{Frame}
    outgoing::Channel{Frame}
end

function Base.getproperty(s::Stream, ϕ::Symbol)
    if ϕ == :state
        getstate(s)
    else
        getfield(s, ϕ)
    end
end

function Base.setproperty!(s::Stream, ϕ::Symbol, v)
    if ϕ == :state
        setstate!(s, v)
    else
        setfield!(s, ϕ, v)
    end
end

getstate(s::Stream) = lock(() -> getfield(s, :state)[], s.state_lock)

setstate!(s::Stream, ψ::StreamState) = lock(() -> (getfield(s, :state)[] = ψ), s.state_lock)

transition!(s::Stream, f::Frame) = (s.state = transition(s.state, f))

function Stream(id::Integer, incoming::Channel, outgoing::Channel, s₀::StreamState=stream_idle)
    Stream(id, ReentrantLock(), s₀, incoming, outgoing)
end

# this method exists becase we need to declare using existing channel in Confluence
function Stream(id::Integer, outgoing::Channel, s₀::StreamState=stream_idle; max_incoming_frames::Real=Inf)
    Stream(id, Channel{Frame}(max_incoming_frames), outgoing, s₀)
end

function Stream(id::Integer, s₀::StreamState=stream_idle; max_incoming_frames::Real=Inf, max_outgoing_frames::Real=Inf)
    Stream(id, Channel{Frame}(max_incoming_frames), Channel{Frame}(max_outgoing_frames), s₀)
end

Base.close(s::Stream) = (s.state = stream_closed)

Base.isopen(s::Stream) = s.state ≠ stream_closed

isclientid(id::Integer) = isodd(id)

"""
    isclient(s::AbstractStream)

As per the spec, streams initiated by the client must have odd numbered ID's.
"""
isclient(s::AbstractStream) = isclientid(s.id)

isserverid(id::Integer) = iseven(id)

"""
    isserver(s::AbstractStream)

As per the spec, streams initiated by the server must have even numbered ID's.
"""
isserver(s::AbstractStream) = isserverid(s.id)

state(s::AbstractStream) = s.state

streamid(s::Stream) = s.id

isincomingready(s::Stream) = isready(s.incoming)
isoutgoingready(s::Stream) = isready(s.outgoing)

takeincoming!(s::Stream) = take!(s.incoming)
takeoutgoing!(s::Stream) = take!(s.outgoing)

putincoming!(s::Stream, f::Frame) = put!(s.incoming, f)
putoutgoing!(s::Stream, f::Frame) = put!(s.outgoing, f)

# TODO this seems to be reading zero byte messages!
function Message(s::Stream)
    isopen(s) || error("tried to read message from closed stream $s")
    Message(s.incoming) do f
        transition!(s, f)
        !isopen(s)
    end
end

function Base.read(s::Stream, ::Type{Message})
    m = Message(s)
    @logmsg(Debug6, "read message", stream=s, message=m)
    m
end

function Base.write(s::Stream, fi::FrameIterator)
    isopen(s) || error("tried to write frames to closed stream $s")
    for f ∈ fi
        putoutgoing!(s, f)
        transition!(s, f)
    end
end

function Base.write(s::Stream, m::Message; kwargs...)
    if s.state == stream_legacy
        putoutgoing!(s, legacyframe(m))
    elseif s.state == stream_closed
        error("tried to write to closed stream $s")
    else
        write(s, FrameIterator(m, streamid(s); kwargs...))
    end
    @logmsg(Debug6, "wrote message", stream=s, message=m)
end


# do we want to do stream priority stuff in here?
struct Confluence{𝒮<:AbstractStream}
    lock::ReentrantLock
    outgoing::Channel{Frame}
    streams::Dict{Int,𝒮}
end

Base.close(γ::Confluence) = lock(() -> foreach(close, values(γ.streams)), γ.lock)

Base.lock(f, γ::Confluence) = lock(f, γ.lock)

function Confluence(::Type{𝒮}=Stream, outgoing::Channel=Channel{Frame}(Inf)) where {𝒮<:AbstractStream}
    Confluence{𝒮}(ReentrantLock(), outgoing, Dict{Int,𝒮}())
end

function Base.setindex!(γ::Confluence, s::AbstractStream, id::Integer)
    lock(γ.lock) do
        γ.streams[streamid(s)] = s
    end
end
Base.setindex!(γ::Confluence, s::AbstractStream) = setindex!(γ, s, streamid(s))

Base.getindex(γ::Confluence, id::Integer) = lock(() -> γ.streams[id], γ.lock)

Base.delete!(γ::Confluence, id::Integer) = lock(() -> delete!(γ.streams, id), γ.lock)

_newclientid(ks) = isempty(ks) ? 1 : max(1, maximum(n -> (isclientid(n) ? n : 1) + 2, ks))
_newserverid(ks) = isempty(ks) ? 2 : max(2, maximum(n -> (isserverid(n) ? n : 2) + 2, ks))

function _create_stream!(γ::Confluence{𝒮}, 𝒻::Function, s₀::StreamState=stream_idle) where {𝒮}
    lock(γ.lock) do
        id = 𝒻(keys(γ.streams))
        γ.streams[id] = 𝒮(id, γ.outgoing, s₀)
    end
end

create!(γ::Confluence, id::Integer, s₀::StreamState=stream_idle) = _create_stream!(γ, _ -> id, s₀)
create!(γ::Confluence, ::typeof(isclient), s₀::StreamState=stream_idle) = _create_stream!(γ, _newclientid, s₀)
create!(γ::Confluence, ::typeof(isserver), s₀::StreamState=stream_idle) = _create_stream!(γ, _newserverid, s₀)

function _get_stream!(ℊ, γ::Confluence{𝒮}, 𝒻::Function, s₀::StreamState=stream_idle) where {𝒮}
    s, isnew = lock(γ.lock) do
        id = 𝒻(keys(γ.streams))
        isnew = id ∉ keys(γ.streams)
        get!(() -> 𝒮(id, γ.outgoing, s₀), γ.streams, id), isnew
    end
    isnew && ℊ(s)  # if it's a new stream, we want to run, usually a @spawn *inside* ℊ
    s
end
_get_stream!(γ::Confluence, 𝒻::Function, s₀::StreamState=stream_idle) = _get_stream!(_ -> nothing, γ, 𝒻, s₀)

Base.get!(γ::Confluence, id::Integer, s₀::StreamState=stream_idle) = _get_stream!(γ, _ -> id, s₀)
Base.get!(γ::Confluence, f::Frame, s₀::StreamState=stream_idle) = get!(γ, streamid(f), s₀)
Base.get!(γ::Confluence, f::Frame{<:LegacyPayload}, s₀::StreamState=stream_legacy) = get!(γ, streamid(f), s₀)
Base.get!(γ::Confluence, ::typeof(isclient), s₀::StreamState=stream_idle) = _get_stream!(γ, _newclientid, s₀)
Base.get!(γ::Confluence, ::typeof(isserver), s₀::StreamState=stream_idle) = _get_stream!(γ, _newserverid, s₀)
Base.get!(ℊ, γ::Confluence, ::typeof(isclient), s₀::StreamState=stream_idle) = _get_stream!(ℊ, γ, _newclientid, s₀)
Base.get!(ℊ, γ::Confluence, ::typeof(isserver), s₀::StreamState=stream_idle) = _get_stream!(ℊ, γ, _newserverid, s₀)
Base.get!(ℊ, γ::Confluence, id::Integer, s₀::StreamState=stream_idle) = get!(ℊ, γ, _ -> id, s₀)

putincoming!(γ::Confluence, id::Integer, f::AbstractFrame) = putincoming!(get!(γ, id), f)
putincoming!(γ::Confluence, f::AbstractFrame) = putincoming!(γ, streamid(f), f)
putincoming!(𝒻, γ::Confluence, id, f::AbstractFrame) = putincoming!(get!(𝒻, γ, id), f)
putincoming!(𝒻, γ::Confluence, f::AbstractFrame) = putincoming!(𝒻, γ, streamid(f), f)

putoutgoing!(γ::Confluence, id::Integer, f::AbstractFrame) = putoutgoing!(get!(γ, id), f)
putoutgoing!(γ::Confluence, f::AbstractFrame) = putoutgoing!(γ, streamid(f), f)

takeincoming!(γ::Confluence, id::Integer) = takeincoming!(γ[id])
takeoutgoing!(γ::Confluence, id::Integer) = takeoutgoing!(γ[id])

takeoutgoing!(γ::Confluence) = take!(γ.outgoing)

function Base.close(γ::Confluence, id::Integer)
    lock(γ) do
        s = get(γ.streams, id, nothing)
        isnothing(s) && return
        close(s)
    end
end


function respond(𝒻, s::Stream)
    while isopen(s)
        m = read(s, Message)
        r = 𝒻(m)
        if !(r isa Message)
            error("response function $𝒻 returned $r which is not an HTTP2.Message")
        end
        write(s, r)  # for non-legacy this is expected to close the stream and terminate the loop
        @debug("response message written to stream", stream=s)
    end
end
